
.. index::
   Juridique (Chambre sociale)

.. _chambre_sociale:

==================================
Cour de cassation, chambre sociale 
==================================

.. seealso:: 

   - https://fr.wikipedia.org/wiki/Chambre_sociale_de_la_Cour_de_cassation_fran%C3%A7aise

La chambre sociale de la Cour de cassation française est la formation de cette 
juridiction consacrée aux affaires sociales.


Les principales attributions de la chambre sociale sont les suivantes :

- Droits et obligations des parties au contrat de travail.
- Rupture du contrat de travail dont licenciement économique et disciplinaire.
- Relations collectives du travail.
- Élections en matière sociale et professionnelle, internes à l’entreprise.
- Représentation du personnel et protection des représentants du personnel.
- Entreprises à statut.
- Droit de l’emploi et de la formation.
- Situation économique et droit de l’emploi (notamment licenciement économique).
- Interférence du droit commercial et du droit du travail.
- Droit communautaire du travail.


.. toctree::
   :maxdepth: 4
   
   arrets/index

   
