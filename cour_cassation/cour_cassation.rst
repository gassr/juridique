
.. index::
   Juridique (Cour de cassation)

.. _cour_de_cassation:

=================
Cour de cassation 
=================

.. seealso:: 

   - https://fr.wikipedia.org/wiki/Cour_de_cassation_%28France%29

La Cour de cassation est la juridiction la plus élevée de l'ordre judiciaire 
français. Elle est le pendant du Conseil d'État, dans l'ordre administratif. 
C'est une juridiction permanente, qui siège au Palais de Justice de Paris, 
au niveau du 5 quai de l'Horloge.

La Cour de cassation comprend six chambres :

- cinq chambres civiles (dont une chambre commerciale et une chambre sociale, 
  et trois chambres civiles spécialisées respectivement en droit des personnes, 
  de la famille et des contrats ; en responsabilité civile et sécurité sociale ; 
  en droit immobilier et droit de la construction)
- une chambre criminelle.



.. toctree::
   :maxdepth: 4
   
   chambre_sociale/index

   
