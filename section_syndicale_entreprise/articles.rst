
.. index::
   pair: RSS; Article L2142-1 



=======================================
articles du Code du travail sur le RSS
=======================================


Article L2142-1
===============

Dès lors qu'ils ont plusieurs adhérents dans l'entreprise ou dans 
l'établissement, chaque syndicat qui y est représentatif, chaque syndicat 
affilié à une organisation syndicale représentative au niveau national et 
interprofessionnel ou chaque organisation syndicale qui satisfait aux critères 
de respect des valeurs républicaines et d'indépendance et est légalement 
constituée depuis au moins deux ans et dont le champ professionnel et 
géographique couvre l'entreprise concernée peut constituer au sein de 
l'entreprise ou de l'établissement une section syndicale qui assure la 
représentation des intérêts matériels et moraux de ses membres conformément à 
l'article L. 2131-1.

Article L2142-1-1
=================

Chaque syndicat qui constitue, conformément à l'article L. 2142-1, une section 
syndicale au sein de l'entreprise ou de l'établissement de cinquante salariés 
ou plus peut, s'il n'est pas représentatif dans l'entreprise ou l'établissement, 
désigner un représentant de la section pour le représenter au sein de 
l'entreprise ou de l'établissement. 

Le représentant de la section syndicale exerce ses fonctions dans le cadre des 
dispositions du présent chapitre. Il bénéficie des mêmes prérogatives que le 
délégué syndical, à l'exception du pouvoir de négocier des accords collectifs. 

Le mandat du représentant de la section syndicale prend fin, à l'issue des 
premières élections professionnelles suivant sa désignation, dès lors que le 
syndicat qui l'a désigné n'est pas reconnu représentatif dans l'entreprise. 

Le salarié qui perd ainsi son mandat de représentant syndical ne peut pas être 
désigné à nouveau comme représentant syndical au titre d'une section jusqu'aux 
six mois précédant la date des élections professionnelles suivantes dans 
l'entreprise.

Article L2142-1-2
=================

Les dispositions des articles L. 2143-1 et L. 2143-2 relatives aux conditions 
de désignation du délégué syndical, celles des articles L. 2143-7 à L. 2143-10 
et des deuxième et troisième alinéas de l'article L. 2143-11 relatives à la 
publicité, à la contestation, à l'exercice et à la suppression de son mandat 
et celles du livre IV de la présente partie relatives à la protection des 
délégués syndicaux sont applicables au représentant de la section syndicale.

Article L2142-1-3
=================

Chaque représentant de la section syndicale dispose d'un temps nécessaire à 
l'exercice de ses fonctions. Ce temps est au moins égal à quatre heures par 
mois. 
Les heures de délégation sont de plein droit considérées comme temps de travail 
et payées à l'échéance normale. ??L'employeur qui entend contester 
l'utilisation faite des heures de délégation saisit le juge judiciaire. ?

Article L2142-1-4
=================

Dans les entreprises qui emploient moins de cinquante salariés, les syndicats 
non représentatifs dans l'entreprise qui constituent une section syndicale 
peuvent désigner, pour la durée de son mandat, un délégué du personnel comme 
représentant de la section syndicale. Par disposition conventionnelle, ce 
mandat de représentant peut ouvrir droit à un crédit d'heures. Le temps dont 
dispose le délégué du personnel pour l'exercice de son mandat peut être 
utilisé dans les mêmes conditions pour l'exercice de ses fonctions de 
représentant de la section syndicale.

Article L2143-1
===============

Le délégué syndical doit être âgé de dix-huit ans révolus, travailler dans 
l'entreprise depuis un an au moins et n'avoir fait l'objet d'aucune interdiction, 
déchéance ou incapacité relative à ses droits civiques.
Ce délai d'un an est réduit à quatre mois en cas de création d'entreprise ou 
d'ouverture d'établissement.

Article L2143-7
===============

Les noms du ou des délégués syndicaux sont portés à la connaissance de 
l'employeur dans des conditions déterminées par décret. Ils sont affichés sur 
des panneaux réservés aux communications syndicales.
La copie de la communication adressée à l'employeur est adressée simultanément 
à l'inspecteur du travail.
La même procédure est appliquée en cas de remplacement ou de cessation de 
fonctions du délégué.


