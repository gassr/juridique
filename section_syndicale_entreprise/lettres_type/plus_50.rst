


===============================================================================
Lettre type de désignation d’un RSS dans une entreprise de plus de 50 salariés 
===============================================================================

::

	À VILLE, le... 200..

	RECOMMANDE avec ACCUSE DE RÉCEPTION

	Madame/Monsieur
	Société …
	Adresse…

	Objet : désignation d’un représentant de la section syndicale

	Monsieur, Madame,

	Conformément à l’article L2142-1-1 du Code du travail, nous vous informons 
	de la désignation comme représentant de la section syndicale CNT de M, Mme, 
	Melle XXX  pour l’entreprise (ou l’établissement) XXX

	Vous voudrez bien afficher une copie de la présente dans l’entreprise et 
	mettre à disposition de la section syndicale CNT un panneau d’affichage.

	Nous vous demandons également un exemplaire de la convention collective 
	dont dépendent les salariés de votre établissement, ainsi qu’un 
	Code du travail comme le prévoit la loi (article L 412-11 du Code du travail).

	Un double de cette lettre de désignation est adressé concomitamment à 
	l'inspecteur du travail.

	Veuillez agréer, Monsieur, Madame, nos salutations les plus distinguées.

	Pour le syndicat CNT (nom du secrétaire du syndicat),
	signature du secrétaire de syndicat.


