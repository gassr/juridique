


===============================================================================
lettre type de désignation d’un RSS dans une entreprise de moins de 50 salariés
===============================================================================

::

	A VILLE, le... 200..

	RECOMMANDE avec ACCUSE DE RÉCEPTION

	Mme/Monsieur XXX
	Société…
	Adresse…
	Objet : désignation d’un délégué du personnel comme représentant de la section syndicale

	Monsieur, Madame,

	Conformément à l'article L2142-1-4 du Code du travail, nous vous informons 
	que nous désignons XXX , à ce jour délégué du personnel titulaire, comme 
	représentant de la section syndicale CNT, pour toute la durée de son mandat, 
	pour l’entreprise (ou l’établissement) XXX

	Vous voudrez bien afficher une copie de la présente dans l’entreprise et 
	mettre à disposition de la section syndicale CNT un panneau d’affichage.

	Nous vous demandons également un exemplaire de la convention collective 
	dont dépendent les salariés de votre établissement, ainsi qu’un Code du 
	travail comme le prévoit la loi (article L 412-11 du Code du travail).

	Un double de cette lettre de désignation est adressé concomitamment à 
	l'inspecteur du travail.

	Veuillez agréer, Madame, Monsieur le directeur d’entreprise, nos 
	salutations les plus distinguées.

	Pour le syndicat CNT (nom du secrétaire du syndicat),
	signature du secrétaire de syndicat.



