

===========================================
Moyens d'action des representants syndicaux
===========================================


Les représentants syndicaux disposent d'un crédit d'heures payées comme temps 
de travail, ont la liberté de se déplacer à l'extérieur et à l'intérieur de 
l'entreprise et de prendre tous les contacts nécessaires à l'accomplissement 
de leur mission avec les salariés.

Qui bénéficie du crédit d'heures ?

Pour l'exercice de son mandat, chaque délégué syndical dispose d'un crédit d'heures mensuel égal à
- 10 heures dans les entreprises ou établissements occupant de 50 à 150 sala­riés;
- 15 heures dans les entreprises ou établissements occupant de 151 à 500 salariés
- 20 heures dans les entreprises ou établissements occupant plus de 500 salariés. Dans les entreprises ou établissements où, pour chaque section syndicale, sont désignés plusieurs délégués syndicaux, ceux-ci peuvent répartir entre eux le temps dont ils disposent. Ils doivent en informer le chef d'entreprise.

Remarque un délégué syndical ne peut utiliser ses heures de délégation pour assister aux opérations électorales en vue de la désignation des administrateurs des caisses de Sécurité sociale.

L'assistance aux opérations d'un scrutin à caractère national n'entre pas dans le cadre de leur réunion (Cass. soc. 12 novembre 1987, Sté Jynoi Aluminium France c/Resch).

Le délégué syndical central désigné dans les entreprises d'au moins 2 000 salariés qui comporterait au moins deux établissements de 50 salariés chacun ou plus dispose d'un crédit de 20 heures par mois pour l'exercice de ses fonctions. Ces heures s'ajoutent à celles dont il peut disposer à un titre autre que celui de délégué syndical d'établissement. Il peut donc cumuler ce crédit mensuel de 20 heures avec les crédits d'heures auxquels il a droit en qualité de délégué du personnel ou de membre du comité d'entreprise.

Le délégué syndical supplémentaire institué dans les entreprises d'au moins 500 salariés dispose, dans les mêmes conditions que les autres délégués syndicaux, du crédit d'heures légal pour la tranche d'effectif correspondante.

Le délégué du personnel désigné comme délégué syndical dans une entreprise de moins de 50 salariés peut librement utiliser le crédit d'heures dont il dispose au titre d'élu du personnel pour exercer ses fonctions de délégué syndical (C. trav, art. L. 4]2-]]). En cas de circonstances exceptionnelles, ce crédit d'heures pourra être dépassé au titre de l'un ou l'autre de ces mandats.
Quelle utilisation du crédit d'heures ?

Les représentants du personnel doivent utiliser leur crédit d'heures pour l'exercice de leurs fonctions et rien d'autre.

S'agissant des délégués syndicaux, leur mission consiste à représenter le syndicat auprès du chef d'entreprise et à animer la section syndicale.

Aussi, n'entrent pas dans le cadre de leur crédit d'heures :

- l'assistance aux opérations d'un scrutin à caractère national (Cass s Oc. 12 novembre 1987, Sté Reynolds Aluminiun France c/Resch)

- l'assistance aux audiences devant le tribunal d'instance dans le cadre d'une action en contestation d'un protocole d'accord établi en vue des élections du personnel  Si la mission des délégués syndicaux comporte la négociation d'accords avec l'employeur, elle n'emporte pas en elle-même pouvoir d'agir en justice pour assurer le respect de la procédure des élections professionnelles. Pas question non plus d'invoquer des “ circonstances exceptionnelles” (Cass. soc. 2juin 1993, Sté Alcatel espace c/Queiihn).

La mission des délégués syndicaux qui consiste à représenter le syndicat dans l'entreprise peut être exercée en tout lieu, dans l'intérêt des salariés de l'entreprise ou de l'établissement au titre desquels ils ont été désignés, dès lors qu'elle entre dans le cadre de l'objet défini par l'article L. 411-1 du Code du travail. Or, que dit l'article L. 411-1 ? Que les syndicats ont exclusivement pour objet l'étude et la défense des droits, ainsi que les intérêts matériels et moraux, tant collectifs qu'individuels, des personnes visées par leurs statuts.

Ainsi est dans le cadre de sa mission le délégué syndical qui participe à une manifestation tenue à la Bourse du travail lors d'une visite du chef de l'Etat dans la ville où est située l'entreprise, et impute le temps passé sur son crédit d'heures. Cette démarche, affirmait-il, avait trait à la défense de l'emploi dans une entreprise (Cass. soc. 23 janvier 1990, Sté Les Cables de Lyon c/Balaguer).

De même, sont dans l'exercice régulier de leurs fonctions, des délégués syndicaux qui se rendent dans une entreprise en grève dans la mesure où cette grève n'est pas étrangère aux occupations du personnel de l'entreprise dont ils sont les salariés (Cass. soc. 10 juillet 1990, CRCAF du Var c/Denans).

Les délégués syndicaux peuvent aussi utiliser leur heures de délégation pour assister à des audiences judiciaires mettant en cause d'autres représentants du personnel à l'occasion d'un conflit collectif (Cass. soc. ]T avril 1992, SA Automobiles Citmé'i c/Bonnin et Juhen).
Paiement du crédit d'heures

Les heures de délégation sont, de plein droit, considérées comme temps de travail et payées à l'échéance normale. Les délégués syndicaux, comme d'ailleurs les autres représentants du personnel, sont donc présumés bien utiliser leur crédit d'heures.

Les heures de délégation prises en dehors de l'horaire de travail en raison des nécessités du mandat doivent être payées comme heures supplémentaires (Cass. soc. î2jévner 199], Sté Douez et Lambin c/Clabeau et a). En revanche, si le salarié utilise ses heures de délégation pendant les congés payés, il ne peut cumuler leur paiement avec l'indemnité de congés payés (Cass. soc. 19 octobre 1994, Sujol c/Clinique des Cèdres). Les heures de délégation prises en heures supplémentaires ouvrent droit également au repos compensateur (Cass. soc. 13 décembre 1995, Sté Pavillon de la Mutualité c/Bérard).

Du fait de l'exercice de sa mission, le représentant du personnel ne doit subir aucune perte de rémunération et ses heures de délégation doivent être prises en compte pour calculer la durée des congés payés.

En aucun cas les représentants du personnel ne peuvent percevoir un salaire inférieur à celui qu'ils auraient gagné s'ils avaient effectivement travaillé pendant leurs heures de délégation et ne peuvent être écartés de l'application des dispositions d'une convention collective ou d'un règlement intérieur au motif qu'ils étaient absents pour exercer leur mandat (Cass. soc. 22 avril 1997, URSSAF des Bouches-du-Rhône c/Hadoni et a.):

Un délégué syndical qui assure un service normal de nuit a donc droit au titre des heures de délégation au paiement des indemnités pour travail de nuit prévues par la convention collective (Cass. soc. 28 mars 1989, Genai-Simon et Union locale CGT Nice centre c/La Clinique des Sources).

De même, doivent être prises en compte dans le calcul des sommes ducs au titre des heures de délégation les primes versées au titre de l'indemnité compensatrice de panier de nuit et de la majoration pour travail de nuit, ces primes n'étant pas destinées a un remboursement de frais réellement exposés mais faisant l'objet d'un versement forfaitaire ; ayant le caractère d'élément de salaire, elles doivent être prises en compte dans le calcul des sommes dues au titre des heures de délégation. Dans cette affaire, le salarié avait choisi de récupérer, pendant les horaires de nuit, les heures antérieures consacrées, en dehors des horaires de travail, a l'exercice de son mandat représentatif (Cass. soc. ?9laiiyiei 1992, J/~I(Française des Plieitm atiqi les Michelin c/Gibert)

- les sommes représentant la prime de casse-croûte versée aux salariés effec­tuant leur journée de travail d'une seule traite pendant au moins ô heures (Cass. soc. 2 9jarn'ier 1992, Vlan li(1cNireji~aJiÇaL~e (les Fn ci îniatiqi ic £~h< h cliii c/Ch esije)

- les primes de panier allouées aux salariés en raison de l'horaire continu 
  (le travail lorsque ces primes ne constituent pas un remboursement de frais 
  réellement exposés mais la compensation d'une sujétion particulière (Cass s 
  o('. ir avril 1992, Sté Devoiselle c/Lermq).

Si l'employeur conteste l'utilisation du crédit d'heures ?

Avant de contester la régularité de l'utilisation des heures de délégation, 
l'em­ployeur doit d'abord les payer. Le non-paiement de ces heures a l'échéance 
normale l'expose au versement de dommages intérêts pour résistance hâtive 
(Cass. soc. 18juin 1997, Guilmoto c/Polyclinique Le Lanedo).

Ensuite, il doit demander aux représentants du personnel bénéficiaires du versement, de préciser les activités exercées pendant leur temps de délégation. Mais, le salarié n'est tenu que d'indiquer les activités au titre desquelles ont été prises les heures de délégation et non de justifier de leur utilisation (C¼ss. soc. 22 avril 1992, Ferrera c/Entreprise Baylion BatimenLv).

Si les représentants du personnel refusent de lui fournir cette indication, l'em­ployeur doit le leur demander par voie judiciaire, au besoin en saisissant la for­mation des référés du conseil de prud'hommes (Cass. soc. 8juillet 1992, SA Grands Magasins de la Samaritaine ÛMahaux et a.).

L'employeur ne peut, en effet, saisir les juges d'une action en rembourse­ment d'heures de délégation prétendument mal utilisées, qu'après avoir, préa­lablement, demandé aux représentants du personnel l'indication des activités pour lesquelles ces heures ont été utilisées.

L'employeur qui sans avoir au préalable demandé des précisions quant aux activités litigieuses, saisirait directement le conseil de prud'hommes d'une demande de justificatifs ou d'une demande de remboursement des heures qu'il estime injustifiées, verrait ses demandes rejetées et pourrait être condamné pour délit d'entrave ainsi qu'à versement de dommages et intérêts (Cass. soc.). 4 décembre 1991, SBG c/Coustel).

L'employeur aura ensuite la charge d'établir devant les juges, à l'appui de sa contestation, la non-conformité de l'utilisation de ce temps avec l'objet du mandat représentatif
Les frais de déplacement sont-ils à la charge de l'employeur en sus du crédit d'heures ?

Légalement, l'employeur n'est pas tenu de rémunérer le temps des trajets effectués par les délégués syndicaux pour se rendre aux réunions du chef d'entreprise, ce temps n'étant pas assimilé au temps d'exercice des fonctions syndicales. Bien entendu, une convention collective ou un usage obligatoire pourraient imposer à l'employeur une telle indemnisation (Cass. soc. 28 avril 1988, Sté Transports Rapides du Nord c/Legrand).

Sauf usages contraires, l'employeur n'est pas non plus obligé de rembourser à un délégué syndical les frais de déplacement qu'il peut engager pour se rendre à des réunions organisées par l'inspecteur du travail (Cass. soc. 20 octobre 1988, Gamex c/Beauvaîs).

Fiscalité des dépenses du délégué syndical

Les dépenses exposées par un délégué syndical a l'occasion de l'exercice de son mandat ont le caractère de frais professionnels (rep. minis. à M. Lc~ CO(1(lic, J) IN, ]';ft)'1~ieJ ]9~~3, ~. 674). En conséquence, si le délégué syndical a opté pour la déduction des frais réels, il peut déduire ces dépenses pour leur montant exact, sous réserve d'en justifier et d'ajouter a son salaire l'ensemble des allocations pour frais ou remboursement de frais éventuellement perçues de l'employeur ou du syndicat.

Si le délégué syndical a opté pour la déduction forfaitaire de 10 (%) pour frais professionnels, les allocations destinées a couvrir les dépenses supplémentaires entraînées par son activité syndicale sont exonérées d'impôt sur le revenu si elles sont utilisées effectivement conformément a leur objet, par application de l'article 81~1o du Code général des impôts.
Le crédit d'heures peut-il être dépassé ?

Les délégués syndicaux ont la possibilité de dépasser le crédit d'heures légal en cas de circonstances exceptionnelles mais le Code du travail ne donne aucune indication sur ce qu'il y a lieu d'entendre par “ circonstances exceptionnelles ”. Il ne peut s'agir que d'une décision au cas par cas. Ainsi, la pré­paration d'un accord d'entreprise, aurait-il lui-même un caractère exceptionnel, n'est pas de nature a justifier un dépassement du crédit d'heures (Cass. soc. S ii~enibi~ 1987, SA Ciapem c/CheJY),i et a.). Ont justifié, en revanche, le dépassement du crédit d'heures de maniere exceptionnelle les ten­tatives de renégociation d'un accord dénoncé par l'employeur pendant le délai de prévenance (Cass. soc. 28Iiiiii 1989, 54 ABS SF~fCA (ÙLc1)illcIlJ etRi~'iùe). La Cour de cassation a précisé par ailleurs qu'il n'est pas néces­saire que l'événement qui provoque un surcroît de réunions et de démarche soit soudain (Cass. soc. 6juillet 1994, C~é(ht L~'oiiiiai.~ (;À~Iai.~oiiJia~~c).

Exemple : l'employeur doit payer au salarié un congé exceptionnel de cinq jours pour participer au congrès de son syndicat, celui-ci agissant dans le cadre de son mandat (Cass. soc. 28fé~rier I 99~, CRAN! de Normandie c/Chambrela}i>.

Si l'employeur conteste l'existence même des circonstances exceptionnelles invoquées par les délégués syndicaux, et éventuellement l'importance des heures de délégation prises à cette occasion, il rie sera pas tenu (le payer les heures de dépassement à l'échéance normale ; il appartiendra alors aux délégués syndicaux de saisir le conseil de prud'hommes et de prouver que les circonstances étaient bien exceptionnelles et que les heures de dépassement ont été utilisées conformément à la mission (Cass s Oc. 26 /é~'i~iei i 99Jacqiieson et a. cÊ~kin i iIactureji~nçaise des Pneumatiques iVildi cliii).

Or, ce qui constitue une circonstance exceptionnelle justifiant le dépassement du crédit d'heures légal pour un délégué syndical ne constitue pas for­cément une circonstance exceptionnelle pour un délégué du personnel et vice versa.
Les heures de délégation doivent-elles figurer sur le bulletin de paie ?

Il est interdit de faire figurer sur le bulletin de paie des indications concernant l'activité de représentation du personnel.

S'agissant de la rémunération des heures de délégation et de son incidence sur le contenu du bulletin de paie, deux hypothèses sont à distinguer :

- les heures de représentation sont prises sur l'horaire de travail légal ou conventionnel. Dans ce cas, elles sont intégrées dans la durée du travail et ne doivent donc pas être distinguées des autres heures de travail effectif, ni en termes de durée du travail ni en termes de rémunération;

- les heures de représentation sont prises en dehors de la durée du travail ou conventionnelle. Payées comme temps de travail, elles ne doivent pas non plus être identifiables sur le bulletin de paie. Il convient donc de comptabiliser ces heures mais sans préciser leur origine exacte et en les incluant dans une rubrique générale telle que “ autres heures ”.

- L'employeur doit établir une fiche annexée au bulletin de paie qui a la même valeur juridique que celui-ci et qui fournit toutes les informations nécessaires (nature et montant de la rémunération de l'activité de représentation).

Cette fiche permettra de préciser le contenu de la rubrique “autres heures” et notamment d'identifier les heures de délégation et de les distinguer des heures indemnisées à d'autre titre telles que les heures de pause ou d'as­treinte.

Cette fiche peut regrouper l'information sur les heures de représentation avec celle prévue a l'article D. 212-11 du Code du travail sur le repos compensateur. Sur le contenu de cette fiche annexe, le ministre du Travail a précisé que :

- dans la mesure où il existe, dans certaines entreprises, un consensus entre les partenaires sociaux pour que l'activité des représentants du personnel ne fasse pas l'objet de contrôle, système que ces entreprises ne souhaitent pas remettre en cause, il peut être admis de faire figurer sur la fiche annexe, qui reste obligatoire dès lors que le bulletin de paie n'est pas codifié, les mentions suivantes “crédit d'heures légal” ou “ crédit d'heures conventionnel ”

- Si les heures sont prises sur l'horaire de travail légal ou conventionnel, elles don­nent simplement lieu à maintien du salaire et il n'est pas nécessaire de faire apparaître distinctement le montant de leur rémunération sur la fiche annexe. La men­tion “ maintien du salaire ” suffit

- Si les heures sont prises en dehors de la durée du travail légale ou conventionnelle, elles figurent sur le bulletin de paie sous la rubrique “ autres heures ” et le montant de leur rémunération doit être reporté sur la fiche annexe.

Liberté de déplacement

Pour l'exercice de leurs fonctions, les délégués syndicaux peuvent, durant les heures de délégation, se déplacer hors de l'entreprise.

Ils peuvent également, tant durant les heures de délégation qu'en dehors de leurs heures habituelles de travail, circuler librement dans l'entreprise et y prendre tous contacts nécessaires à l'accomplissement de leur mission, notam­ment auprès d'un salarié à son poste de travail, sous réserve de ne pas apporter de gêne importante à l'accomplissement du travail des salariés (C. tia~'., art. L. 412-17).

L'intervention d'un délégué syndical qui se déplace dans les ateliers pour se faire remettre par les salariés un questionnaire sur des revendications concernant la modification du statut collectif des salariés entre dans le cadre de ses attributions. Il appartient aux juges d'apprécier souverainement Si le déplacement est susceptible de porter une gêne importante à l'accomplisse­ment du travail des salariés (Cass. crim. 27 septembre 1988, Petit).

L'employeur ne peut pas interdire l'accès de certains locaux professionnels aux délégués syndicaux par l'utilisation d'un dispositif comprenant un code d'accès et impliquant pour eux de décliner leur identité et le but de leur visite pour pénétrer dans les lieux (Cass. crim. 28juin ]994~ SA VVacles).
Y a-t-il une limite à la libre circulation dans l'entreprise ?

Seule limite  la gêne importante qui pourrait être apportée au travail des salariés.

Pour apprécier cette notion de gêne importante, deux éléments peuvent être pris en considération : d'une part, la nature du travail effectué et, d'autre part, la durée de l'entretien. La durée de l'entretien devra être plus réduite lorsque, par exemple, le salarié est en contact avec la clientèle ou le public à une heure d'affluence.

Documents à communiquer aux délégués syndicaux

Un certain nombre de documents doit être communiqué aux délégués syn­dicaux ; nous les avons recensés par ordre alphabétique (source : SOCIAL PRATIQUE - N° 262 - 10 ).
Bilan social - Les délégués syndicaux reçoivent communication du projet de bilan social dans les mêmes conditions que les membres des comités d'entreprise, c'est-à-dire quinze jours au moins avant la réunion au cours de laquelle le comité d'en­treprise ou le comité d'établissement donnera son avis sur le projet (C. trav, ai~t. L. 438-5). Le représentant syndical au comité d'entreprise pourra ainsi éventuellement présenter les observations du syndicat au comité d'entreprise ou d'établissement.

Convention et accord collectif - L'employeur lié par une convention ou un accord collectif de travail doit en procurer Lin exemplaire aux délégués syndicaux. Il leur fournit également chaque année la liste des modifications apportées. En outre, lorsqu'il démissionne d'une organisation signataire, il en informe sans délai les délégués syndicaux (C. trav'., ai~t. L. 135-7 et L. 135-8).

Égalité professionnelle des hommes et des femmes - Les délégués syndicaux reçoivent communication du rapport annuel de l'employeur sur la situation comparée des conditions générales d'emploi et de formation des femmes et des hommes dans l'entreprise (C. trav, art. L.432-3-1) ô~oirp. 46).

Plan de formation du personnel - Le chef d'entreprise doit communiquer aux délégués syndicaux, au moins une fois par an et trois semaines avant la réunion du comité d'entreprise, les documents d'information obligatoires remis au comité d'entreprise, pour lui permettre d'élaborer le plan de formation du personnel (C t7av.~ art. L. 933-3).

Travail à temps partiel - Au moins une fois par an, l'employeur doit communiquer aux délégués syn­dicaux un bilan du travail à temps partiel réalisé dans l'entreprise, portant notamment sur le nombre, le sexe et la qualification des salariés concernés (C. trav. art. L. 212-4-5) ½oirp. 46).

Rémunération et octroi d'avantages sociaux

Constitue une discrimination syndicale, le refus d'attribution d'une prime à un délégué syndical, deve­nu permanent syndical en raison du non-exercice de ses fonctions et de l'exercice par lui d'une activité syndicale (Cass. soc. 24 février 1993, Crim. Sud-Est c/Cahano et autre).

Constitue par ailleurs un délit d'entrave à l'exercice du droit syndical, le refus d'octroyer des tickets restaurant en raison d'absences pour activités syndicales (Cass. crim. 30 avril 1996, Valat). De même, doit être sanctionné, le fait pour un employeur de prendre en considération le caractère contestataire de salariés titulaires de mandats syndicaux pour ne pas leur attribuer une prime exceptionnelle (Cass. soc. 7 novembre J990, G£4W!EWc/Gautier et autres).

Mesures de discipline

Ont été condamnés pour atteinte à la liberté syndicale

Un employeur qui avait donné à une salariée un avertissement pour carence de travail, alors que cette salariée, qui avait été désignée comme déléguée syndicale depuis douze jours, n'avait encouru aucun reproche depuis 18 ans et qu'il n'était pas, en outre, prouvé que les malfaçons constatées lui étaient imputables (Cass. crim. J 7jévrier 198], Karakozian).

L'employeur qui inflige un blâme à une employée candidate non élue aux fonctions de délégué du personnel, qui avait apposé une affiche extraite d'une revue syndicale sur le panneau réservé aux délégués du personnel ; la salariée avait refusé de justifier de la qualité en vertu de laquelle elle avait apposé ce document. Les juges ont estimé que l'employeur n'avait pas le droit d'exiger une telle justification et que la mesure disciplinaire avait été prise en considération de l'activité syndicale de la salariée dans l'entreprise (Cass. crim. 24 octobre 1978, Massias).

L'employeur qui inflige un blâme à un délégué syndical ayant écrit aux dirigeants de son entreprise, dans des termes mesurés pour s'étonner du défi­cit du résultat d'exploitation d'une année sur l'autre (CA de Montpellier, 24 juin 1993, Gautier c/Centre de gestion et d 'économie rurale de l'Aude).

Mesures de licenciement

En pratique, il est exceptionnel que le licenciement d'un salarié soit officiellement motivé pour activité syndicale, ce qui entraîne sa nullité en cas de contestation (CA Paris février 1995, £tri c/SA -SRG). C'est donc aux tribunaux de rechercher, en cas de contestation, quelle est la cause réelle du congédiement. Ont été ainsi jugés sans cause réelle et sérieuse, les licenciements

- d'une employée qui venait d'être nommée comme secrétaire adjointe d'un syndicat, alors que son prédécesseur avait été congédié dans les mêmes conditions (Cass. soc. 23 mars 1960, Sté COOF de l’Hérault et autre c/ÊVftiiet)

- d'un salarié qui avait distribué, hors de l'établissement, un tract syndical (Cass. soc. 7 décembre 1960, Sté “ Aux Galeries ” c/Mégevand)

- d'un salarié concomitant avec la création, à son initiative, d'une section syndicale dans l'entreprise (Cass. soc. 2 avril 1987, Sté Entreprise orléa­naise de travaux publics c/Gaubert).

Les chefs d'établissements, directeurs ou gérants, qui portent atteinte à la liberté syndicale, sont passibles d'une amende de 25 000 F (C. trav., art. L. 481-3) et, en cas de récidive, d'un emprisonnement d'un an et d'une amen­de de 50 000 F ou de l'une de ces deux peines seulement.


       
    




