



==============================
Introduction
==============================

.. seealso::
 
   - https://fr.wikipedia.org/wiki/Section_syndicale_d%27entreprise
   - http://www.cnt-f.org/59-62/archives/syndic.htm
  
La loi « portant rénovation de la démocratie sociale et réforme du temps de 
travail », publiée au Journal officiel et donc effective depuis le 20 août 2008, 
refonde le droit syndical dans les entreprises du secteur privé.

Sans rentrer dans les débats politiques et idéologiques sur les motivations 
générales de cette loi, largement critiquables [note du webmaster : pour en 
savoir plus à ce sujet, cliquer ici], il est un élément - le représentant de 
la section syndicale (RSS) - qui pour la CNT constitue une certaine forme 
d’avancée. En effet, avant, n’existait que le délégué syndical : il fallait 
clandestinement créer une section syndicale, s'assurer de remplir des critères 
de représentativité (activité, ancienneté et nombre d’adhérents notamment, 
tout ça à obtenir... clandestinement) et déclarer la section en étant très 
souvent attaqué devant le tribunal d'instance pour non-représentativité.

Maintenant, la CNT peut engager une action syndicale d’entreprise plus 
facilement en désignant un RSS et ainsi une section syndicale avec des droits 
de base (cf. annexes 2 et 3) sans qu'elle ait obtenu la représentativité 
proprement dite, mais en remplissant tout de même des critères restreints 
(cf. annexe 1).

Voici donc une fiche pratique juridique sur le nouveau droit d’implantation 
syndicale dans une entreprise du secteur privé et notamment le RSS. 

Vous trouverez en annexe l’ensemble des articles du Code du travail mentionnés 
ici ainsi que des modèles de désignation de RSS.

Création d’une section syndicale
================================

La constitution d'une section syndicale est depuis la nouvelle loi ouverte à 
tous les syndicats (donc la CNT !), y-compris aux syndicats non représentatifs 
nationalement ou dans la branche d’industrie concernée, avec trois exigences 
(C. trav., art. L. 2142-1) :

- l’indépendance ;
- respect des valeurs républicaines ;
- être affilié à un syndicat légalement constitué depuis au moins deux ans et 
  comprenant l’entreprise dans son champ professionnel et géographique ;
- avoir plusieurs adhérents.

Désignation d’un représentant de la section syndicale
=====================================================

Tant qu’un syndicat n’est pas représentatif dans l’entreprise ou l’établissement, 
il n’est pas en droit de désigner un délégué syndical. En revanche, s’il constitue une section syndicale au sein de cette entreprise ou de cet établissement, il peut désormais désigner un représentant de la section syndicale (RSS) qui aura pour mission de le représenter.

Le RSS doit, comme le délégué syndical, être âgé de 18 ans révolus, travailler dans l’entreprise depuis un an au moins (4 mois en cas de création d’entreprise, 6 mois dans les entreprises de travail temporaire) et n’avoir fait l’objet d’aucune interdiction, déchéance ou incapacité relatives à ses droits civiques (C. trav., art. L. 2143-1 et L. 2143-2).

Dans les entreprises de moins de 50 salariés, le RSS doit être un délégué du personnel et est désigné pour la durée de son mandat (C. trav., art. L. 2142-1-4).

Cette désignation suit les mêmes modalités de publicité (information de l’employeur, affichage, transmission à l’inspecteur du travail) et de contestation (saisine du juge judiciaire dans les 15 jours) que celles des délégués syndicaux (C. trav., art. L. 2143-7 et L. 2143-8) : le RSS peut être contesté devant le tribunal d’instance sur la base de critères évoqués au 1.

Ce mandat est cumulable avec la fonction de DP, d’élu ou de représentant syndical au comité d’entreprise ou d’établissement (C. trav., art. L. 2143-9).

Droits du RSS
=============

Le RSS bénéficie d'un statut et d'attributions proches de ceux du délégué syndical, à l'exception du pouvoir de négocier et conclure des accords collectifs sauf dans une situation bien précise : en l’absence de délégué syndical dans l'entreprise et dans l’hypothèse où il n’a pas été possible de conclure d’accord collectif avec un représentant élu ou un salarié mandaté

Concrètement, le RSS bénéficie d’un crédit d’heures de délégation d’au moins 4 heures par mois (C. trav., art. L. 2142-1-3) et bénéficie des moyens mis à disposition de la section syndicale (affichage, distribution de tract, etc.), notamment le local commun à toutes les sections dans les entreprises de plus de 200 salariés. En revanche, dans les entreprises ou établissements de 1 000 salariés ou plus, l’obligation de mettre un local distinct à disposition de chaque section syndicale est désormais réservée aux sections créées par des syndicats représentatifs (C. trav., art. L. 2142-8).

Le RSS est protégé contre le licenciement de la même manière qu’un délégué syndical (C. trav., art. L. 2142-1-2), c'est-à-dire que son licenciement est soumis, après enquête contradictoire, à l'autorisation de l'inspection du travail, pendant son mandat et un an après la fin de celui-ci.

Dans les entreprises de moins de 50 salariés, le mandat de RSS ouvre droit à un crédit d’heures uniquement si une disposition conventionnelle le prévoit. Le temps dont dispose le délégué du personnel pour l’exercice de son mandat d’élu peut être utilisé dans les mêmes conditions pour l’exercice de ses fonctions de RSS(C. trav., art. L. 2142-1-4).

Fin du mandat de RSS
====================

Le mandat du RSS prend fin, à l’issue des premières élections professionnelles suivant sa désignation. Deux cas sont alors possibles :
- si le syndicat est reconnu représentatif c’est-à-dire qu’il a obtenu au moins 10% des suffrages au premier tour des élections de CE, DUP ou à défaut DP, la section syndicale peut alors désigner un délégué syndical ;
- si le syndicat qui l’a désigné n’est pas reconnu représentatif dans l’entreprise, le RSS ne peut pas être désigné à nouveau comme représentant syndical jusqu’aux six mois précédant la date des élections professionnelles suivantes dans l’entreprise (C. trav., art. L. 2142-1-1). Le syndicat qui demeure non représentatif peut en revanche désigner un autre salarié comme RSS.

Quoi qu’il en soit, l’ex-RSS reste protégé contre le licenciement après son mandat de la même manière qu’un ex-délégué syndical pour une durée de 12 mois : l’autorisation de l’inspecteur du travail doit être donnée. En cas d’acceptation du licenciement par l’inspection du travail, des recours existent encore : recours hiérarchique auprès du ministre du travail, et recours contentieux au tribunal administratif.

Le bureau confédéral de la CNT

PS : Trois mois après la publication de la loi du 20 août 2008 réformant la 
représentativité syndicale, la DGT (direction générale du travail) - dans sa 
circulaire n° 20 du 13 novembre 2008 - fait le point sur cette réforme et sur 
les dates d’entrée en vigueur des nouvelles règles (représentativité dans 
l’entreprise ou l’établissement, désignation des DS et les RSS, protocole 
d’accord préélectoral, salariés mis à disposition...). 

Pour télécharger la circulaire de la DGT (format pdf - 52 pages - 3,67 Mo), 
cliquer ici.


