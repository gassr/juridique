

=======================================================================
Quelques questions-réponses sur le droit syndical dans le secteur privé
=======================================================================


Quelques questions-réponses sur le droit syndical dans le secteur privé :
(attention, la loi et la jurisprudence font évoluer constamment le droit syndical)

Quels sont les droits individuels des salariés ?
=================================================

Le salarié, quels que soient son âge ou sa nationalité, a toute liberté pour 
adhérer au syndicat de son choix ou ne pas se syndiquer (C 1ra~'., ail. L. 41 !-5~). 

Par ailleurs, les anciens salariés qui ont exercé leurs fonctions pendant au 
moins un an peuvent, soit continuer à faire partie d'un syndicat professionnel 
de salariés, soit adhérer à un syndicat professionnel de leur choix (C. trav., ait. L. 411-7).

Le fait qu'un salarié soit ou ne soit pas syndiqué ne doit avoir aucune 
incidence sur son droit à l'emploi ni sur sa carrière. Il est en effet interdit 
à tout employeur de prendre en considération l'appartenance à un syndicat ou 
l'exercice d'une activité syndicale pour arrêter ses décisions en ce qui 
concerne notamment l'embauchage, la conduite et la répartition du travail, 
la forma­tion professionnelle, l'avancement, la rémunération et l'octroi 
d'avantages sociaux, les mesures de discipline et de congédiement (C. trav'., L. 412-2).

Embauchage. Le questionnaire préalable à l’embauchage ne peut contenir de 
question sur l'affiliation syndicale ou l'exercice d'une activité syndicale. 

Insérée dans un questionnaire d'embauche, la question d’une affiliation syndical 
implique par elle-même la prise en considération. cil vue d'un embauchage, 
de l'appartenance à un syndicat ou de l'exercice d'une activité syndicale 
(Cass. soc. 13 mai J969~ CFDT c/SA Rodahie) ce qui est rigoureusement interdit, 
sous peine de lourdes sanctions (amende de 25 OO() ~ C. trav., art. L. 48]-3).

Conditions et répartition du travail. En aucun cas, les conditions d'exé­cution 
du contrat ne doivent être influencées par l'appartenance ou l'activité syndicale 
(Cass. crim. 25 mai 1982, Sté Loca tel c/Giiggeiiheiin et autres). 

Commet le délit d'entrave à la liberté syndicale, l'employeur qui, par des 
mutations successives prises à l'égard d'un cadre, a voulu délibérément 
sanctionner l'appartenance syndicale de ce salarié (Cass. crim. 25 m<ii~ l<)80, 
Sté Laboratoires d'application dermatologique de Vichy (Valeiiola).

Ou encore, commet le délit de discrimination syndicale, l'employeur qui pendant 
plusieurs années a imposé à un représentant du personnel des sujétions ayant eu 
pour finalité essentielle de modifier les conditions d'exercice de ses fonctions 
syndicales dans l'entreprise, de l'isoler de ses collègues ainsi que de le 
soumettre à des mesures discriminatoires (Cass. crim. 4 janvier 1991, 
Lefrançois-Dasse c/Syndicat départemental CFDT des industries chimiques).

Où constituer une section syndicale ?
=====================================

Une section syndicale peut être établie dans toutes les entreprises, quelles 
que soient la nature de leurs activités et leur forme juridique (C. trav., art. L. 412-4). 

Une section syndicale peut donc être constituée dans toute entreprise quel que 
soit son effectif, qu'elle soit industrielle, commerciale ou agricole, dans les 
offices publics et ministériels, les professions libérales, les sociétés civiles, 
les syndicats professionnels, les associations de quelque nature que ce soit 
et les entreprises de travail temporaire.

Les entreprises étrangères sont concernées dès l'instant où leur lieu 
d'exploitation est en France. Peu importent la nationalité et le siège social 
de l'entreprise. Seul compte le lieu où se trouve réuni le personnel.

La constitution d'une section syndicale est-elle obligatoire ?
==============================================================

Il s'agit d'une faculté pour les syndicats, et non d'une obligation. A chaque 
syndicat d'apprécier l'opportunité de constituer une section syndicale.

Combien de sections syndicales ?
================================

Une section par entreprise et par syndicat peut être constituée. 

Les dispositions légales n'envisagent la mise en place d'une section syndi­cale 
qu'au niveau de l'entreprise. En fait, la jurisprudence admet la constitution 
de section syndicale au niveau de l'établissement, à condition que la 
représentativité du syndicat soit appréciée dans le cadre de chaque 
établissement (Cass. soc. JO octobre J 990, Sté chaussures André c/Fournel et autres).

Chaque syndicat ne peut constituer qu'une section syndicale. Il ne peut donc 
établir des sections syndicales catégorielles, même s'il compte des adhérents 
dans plusieurs catégories, ouvriers, techniciens et cadres, par exemple 
(Cass. soc. 2 avril 1981, CPAM (le Nlulhoiise c/Menny repré­sentant UGJCT CGT 
de la CPAM de Mulhouse et autre). 

Bien évidemment, s il s'agit d'un syndicat catégoriel (CEE-CGC par exemple), 
une section syndicale spécifique peut être constituée.

Comment constituer une section syndicale ?
==========================================

Contrairement à ce qui se passe pour les délégués du personnel et le comité 
d'entreprise, l'employeur n'a, en ce domaine, aucune initiative à prendre.

Il appartient à chaque syndicat de décider ou non de constituer une section 
syndicale au sein de l'entreprise. Cette décision n'est soumise à aucune 
condition de forme ni de publicité (Cass. soc. 30 mars 1978, Becquet c/Ets Ménard). 

Elle n'est pas non plus soumise à une condition d'effectif (Cass. soc. 24 
février 1993, Xarero c/Comptoir (le l'économat de l'armée). 

Bien que la loi n'impose aucune formalité, il est souhaitable que les 
organisations syndicales, afin d'éviter toute contestation ultérieure, 
informent l'employeur, par lettre recommandée avec AR, de la constitution, 
dans son entreprise, d'une section syndicale et en adressent un exemplaire à 
l'inspecteur du travail (cir. DRT n° J3 du 30 novembre 1984).

La section syndicale n'a pas la personnalité juridique. Elle ne peut donc agir 
en son nom propre que si elle est constituée en syndicat d'entreprise, ce qui
n'est pas interdit mais ne se rencontre que dans les grandes entreprises.

Moyens de la section syndicale
===============================

La section syndicale dispose d'un certain nombre de moyens:

- collecte des cotisations syndicales;
- affichage des communications syndicales ;
- diffusion de tracts;
- utilisation d'un local dans l'entreprise;
- réunions syndicales dans l'entreprise ;
- crédit d'heures.

Par ailleurs, dans les entreprises d'au moins 50 salariés, chaque syndicat 
représentatif qui constitue une section syndicale a le droit de désigner un 
ou plusieurs délégués syndicaux.

Où et quand collecter les cotisations syndicales ?
===================================================

La collecte des cotisations syndicales peut être effectuée à l'intérieur de 
l'en­treprise (C. trav, art L. 412-7). Elle peut donc avoir lieu pendant ou en 
dehors des heures de travail, dans les locaux de travail ou en dehors.

En principe, la collecte des cotisations syndicales est faite, la plupart du 
temps, par les délégués syndicaux. Elle peut être faite également par tout 
autre salarié, adhérent ou non au syndicat. 

Une seule interdiction: elle concerne l'employeur qui, en aucun cas, ne peut 
collecter les cotisations syndicales ni les prélever sur les salaires de son 
personnel pour les payer aux lieu et place de celui-ci (C. trav., art. L. 412-2).

Pour les salariés ne pratiquant pas la déduction des frais réels et les 
pensionnés, le versement des cotisations versées aux organisations syn­dicales 
représentatives des salariés et des fonctionnaires ouvre droit à une réduction 
d'impôt sur le revenu égale à 30 % du montant des coti­sations versées, montant 
plafonné à 1 % du revenu brut (CCI,, art. 199 Quater C).

Qui peut afficher les communications syndicales ?
==================================================

Le délégué syndical n'a pas l'exclusivité ou la matière, toute personne 
adhérant à la section syndicale peut afficher les communications syndicales. 
Il n'est pas même nécessaire d'être syndiqué dans la mesure où l'on est mandaté 
à cet effet par la section syndicale.

Où afficher les communications syndicales ?
===========================================

Les communications syndicales sont librement affichées sur des panneaux 
réservés à cet usage et distincts de ceux affectés aux communications des 
délégués du personnel et du comité d'entreprise.

Les panneaux d'affichage sont mis à la disposition de chaque section syndicale 
suivant des modalités fixées par accord avec le chef d'entreprise (C.trav., art. L. 412-8).

C'est donc un accord qui fixera l'emplacement de panneaux d'affichage et les 
règles matérielles de leur utilisation. Une fois l'accord conclu, il faudra en 
respecter les termes. L'employeur n'a pas la possibilité de changer 
unilatéralement l'emplacement des panneaux syndicaux, ni d'en réduire le nombre. 

L'emplacement choisi devra rendre possible une lecture facile des 
communications syndicales.

Le temps d'affichage sur les emplacements réservés n'est pas limité. En cas 
d'installation provisoire dans de nouveaux locaux, des panneaux d'affichage 
syndical doivent être mis en place. Cette précision avait été donnée dans 
une affaire où l'installation provisoire avait duré deux ans puis s'était 
encore prolongée pendant trois ans (Cass. crim. 15 octobre 1985., JBM France).

Sauf accord exprès, l'affichage syndical ne peut se faire dans les locaux de 
la cantine (Cass. soc. 9 juin 1983, Donabin c/SA Les Câbles de Lyon et autres).

Que peut-on afficher ?
=======================

Le contenu des affiches est librement déterminé par l'organisation syndicale, 
sous réserve des dispositions relatives à la presse. L'affichage ne doit donc 
être ni diffamatoire, ni injurieux, ne contenir ni violence, ni mentions 
volontairement outrancières.

Quant à la forme de la communication, il peut s'agir aussi bien d'une note, 
d'un communiqué syndical, d'un extrait de presse que d'un article de revue, 
ou même d'une lettre.

L'employeur peut-il contrôler l'affichage ?
===========================================

Un exemplaire des communications syndicales est transmis au chef d'entreprise, 
simultanément à l'affichage (C. trav., art. L. 412-8., alinéa 2), mais 
cette disposition ne confère aucun droit de contrôle au chef d'entreprise.

L'employeur qui conteste le contenu des communications syndicales a la possibilité:

- de saisir le juge des référés auprès du tribunal de grande instance s'il y a 
  urgence, troubles manifestement illicites et absence de contestation sérieuse. 
  Dans ce cas, c'est au juge des référés d'ordonner le retrait immédiat de l'affiche ;
- de saisir le tribunal de grande instance dans les autres cas, notamment 
  lorsque l'affichage litigieux ayant été retiré, l'urgence a disparu.

L'employeur qui, de sa propre autorité, enlèverait des affiches, commettrait le 
délit d'entrave à l'exercice du droit syndical (emprisonnement d'un an et 
amende de 25 000 F ou l'une de ces deux peines seulement) (C. trav., art L. 481-2).

Qui peut diffuser les tracts et publications ?
==============================================

Ce sont souvent des délégués syndicaux, mais toute personne adhérant à la 
section syndicale ou mandatée par elle peut distribuer tracts et publications. 

Des délégués du personnel peuvent être amenés à distribuer des tracts syndicaux 
comme n 'importe quel membre d'un syndicat, mais ils ne peuvent utiliser 
leurs heures de délégation rémunérées par l'employeur pour effec­tuer une telle 
distribution (Cass. soc. 12 mars 1985. Sté Siemens c/Chabanne et Lecharbonnier).

Où distribuer les tracts ?
===========================

Les publications et tracts de nature syndicale peuvent être librement diffusés 
aux travailleurs de l'entreprise dans l'enceinte de celle-ci 
(C. trav., art.L.412-8., alinéa 4).

La diffusion peut avoir lieu, d'une manière générale, en tout lieu de 
l'entreprise, par exemple:

- dans un couloir conduisant aux ateliers ;
- dans un service non accessible au public.

Le hall d'entrée d'un immeuble à usage de bureaux utilisé collectivement par 
les sociétés locataires ne constitue nullement, au sens de l'article L. 412-8, 
" l'enceinte ” des entreprises. 

Est donc abusive la distribution de tracts syndicaux dans le hall d'entrée de 
l'immeuble où transitent les salariés des autres entreprises ainsi que leurs 
visiteurs.

Est également abusive la distribution de tracts aux portes d'un restaurant 
d'entreprise recevant des étrangers à l'entreprise.

L'employeur peut s'opposer au dépôt permanent de documents syndicaux dans 
le hall d'entrée de l'entreprise, dès lors qUe ces documents se trouvent 
ainsi à la disposition, non seulement des employés, mais encore des clients 
(Cass. crim. 30 janvier 1973, Section syndicale CFDT de l'hôpital psychiatrique 
de Cayssiols c/ChanaI).

Quand diffuser les tracts ?
===========================

Les tracts ne peuvent être librement diffusés qu'aux heures d'entrée et de 
sortie du travail (C. trav. art. L. 412-8., alinéa 4).

Interdiction donc de distribuer des tracts pendant les heures de travail ou
 pendant les temps de pause. La Cour de cassation a ainsi pu ordonner la 
 cessation d'une distribution de tracts syndicaux faite alors que les salariés 
 avaient déjà commencé le travail depuis une demi-heure (Cass. soc. 27 mai 1997 
 Syndicat CGT Dassault Falcon service c/Sté Dassault Falcon service).

La distribution de tracts dans l'enceinte de l'entreprise pendant la pause du 
déjeuner est illicite (Cass. soc. 8 juillet 1982, Sté Rebichon-Signode c/Simon 
; Cass. soc. 2<) octobre 1988, Thomas et a. c/SA des Avions Marcel Dassault) (1). 

Est également illicite le dépôt de tracts dans les bureaux, intervenant en 
dehors des heures de travail et pendant l'absence du personnel 
(Cil Paris, 13 janvier 1994, SARL Dassault Falcon service c/Syndicat CGT de 
la Sté Dassault Falcon service).

Dans les entreprises pratiquant les horaires variables, la diffusion des 
documents syndicaux est permise durant les plages mobiles : elle ne peut être 
interdite que durant les plages fixes.

Que peut-on diffuser ?
=======================

Il peut s'agir aussi bien d'une distribution gratuite de tracts que de la vente 
de certains journaux ou revues. Librement déterminé par l'organisation syndicale, 
sous réserve de l'application des dispositions relatives à la presse 
(voir page 15), le contenu des publications et tracts doit conserver cependant 
une nature syndicale et être en rapport avec la mission des syndicats. 

Une projection audiovisuelle peut être assimilée à une publication 
(Cass. soc. 10 juillet 1982, Sté Rebichon Signode c/Simon et Syndicat CGT).

L'employeur ne dispose d'aucun droit de contrôle sur les documents diffusés. 
D'ailleurs, le législateur n'a pas prévu la communication à l'employeur des 
publications et tracts syndicaux diffusés dans l'enceinte de l'entreprise, 
comme c'est le cas pour les documents destinés à être affichés. 
En cas de litige, les règles sont les mêmes qu'en matière d'affichage (voir page 15).

Il ne saurait être reproché à une organisation syndicale de se livrer, dans un 
tract, à une analyse des conséquences des choix politiques sur les intérêts 
économiques et sociaux de ses membres (cir. DRT n° 13 du 30 novembre1984).

Signalons cependant que l'administration admet au contraire la diffusion de 
documents syndicaux aux heures de repas et notamment dans les locaux de cantine 
(Bull. doc. du ministère du Travail n° 58/71 de juin 1971).

En revanche, a été jugé illicite, la distribution dans l'entreprise de tracts 
visant uniquement “ à orienter le choix politique des destinataires” en 
invitant les salariés à voter pour le programme de la gauche lors des élections 
législatives (Cass. crim. 25 novembre 1980, Union départementale CGT d'I1e 
et Vilaine c/Le Vacon).

Par ailleurs, le contenu des tracts ne doit pas consister en des attaques 
personnelles (Cass. crim. 23 novembre 1993, Plaquin) ni porter atteinte à 
l'hon­neur ou à la considération des dirigeants (CA Paris, 18 novembre 1994, 
Bogey et autres c/Fédération française des syndicats des banques et de 
sociétés financières CFDT et autres).

Un local syndical pour qui ?
=============================

La réponse est fonction de la taille de l'entreprise :

Dans les entreprises ou établissements où sont occupés 200 salariés au plus, 
sauf accord contraire, le chef d'entreprise n'est pas tenu de mettre un local 
à la disposition des sections syndicales.

Dans les entreprises ou établissements où sont occupés plus de 200 salariés 
et moins de 1.000 salariés, (1) le chef d'entreprise doit mettre à la 
disposition des sections syndicales un local commun convenant à l'exercice 
de la mission de leurs délégués (C. trav, art. L. 412-9). 
Les modalités d'aménagement et d'utilisation devront être fixées par accord 
avec le chef d'en­treprise.

Le local doit être réservé aux sections syndicales et ne pas être confondu 
avec le local attribué au comité d'entreprise ou aux délégués du personnel. 
Bien entendu, le local doit être mis gratuitement à la disposition des 
sections syn­dicales. Il doit être utilisé librement pour des activités 
syndicales, naturellement.

Dans les entreprises ou établissements où sont occupés au moins 1 000 salariés, 
l'employeur doit mettre à la disposition de chaque section syndicale un local 
convenable, aménagé et doté du matériel nécessaire à son fonctionnement ; 
tables, chaises, armoires, machines à écrire, éventuellement matériel de 
sténographie, de photocopie, et la plupart du temps, téléphone, devront être
mis à la disposition de la section syndicale. Celle-ci pourra adjoindre un 
répondeur à son téléphone, dans la mesure où ce téléphone a lui-même été 
autorisé par l'employeur (Cass. soc. 27 octobre 1981, SARL Sama c/Goujet).

Les modalités d'aménagement et d'utilisation des locaux sont fixées par 
accord avec le chef d'entreprise.

(1) Le nombre de salariés à prendre en compte dans l'effectif s'apprécie 
comme pour l'institution de délégués syndicaux.

Quand se réunit la section syndicale ?
======================================

Les adhérents de chaque section syndicale peuvent se réunir une fois par mois, 
suivant des modalités fixées par accord avec le chef d'entreprise.

La réunion doit avoir lieu en dehors des heures de travail des participants, 
à l'exception des représentants du personnel qui, eux peuvent se réunir sur 
leur temps de délégation (C. trav, art. L. 412-10).

Sous cette réserve (hors du temps de travail des participants), la réunion 
pendant les horaires de travail de l'entreprise est autorisée (cas d'horaires 
mobiles ou de travail en équipes).

Où se réunit la section syndicale ?
===================================

La section syndicale se réunit dans l'enceinte de l'entreprise, en dehors des 
locaux de travail, suivant des modalités fixées par accord avec le chef 
d'entreprise.

Les réunions se tiendront dans le local syndical Si un tel local est attribué, 
soit en vertu de la loi (voir p. 18), soit en vertu de dispositions 
conventionnelles ; sinon (cas des entreprises de 200 salariés au plus), 
les réunions syndicales se tiendront dans un local mis à la disposition de 
la section syndicale par l'employeur (la cantine, par exemple) ou par les 
personnes qui en sont les utilisateurs habituels : ainsi, le comité d'entreprise 
peut prêter son local, à condition toutefois d'avoir obtenu l'accord de la 
direction.

La section syndicale peut-elle inviter des personnalités extérieures ?
=======================================================================

Les sections syndicales ont la possibilité d'inviter des personnalités 
extérieures à participer à des réunions :

- s'il s'agit de personnalités syndicales extérieures (1) invitées à une 
  réunion organisée dans le local syndical, nul besoin de demander 
  l'autorisation du chef d'entreprise ; l'employeur qui s'opposerait à leur 
  entrée commettrait le délit d'entrave (Cass. crim. 11 mai 1989, Rousseau) ;

- s'il s'agit de personnalités syndicales extérieures invitées à une réunion 
  se tenant dans un local seulement mis à disposition (cas dans les entreprises 
  de moins de 201 salariés, en l'absence d'accord affectant un local syndical 
  permanent), l'accord du chef d'entreprise est nécessaire.

s'il s'agit de personnalités extérieures non syndicales, l'accord du chef 
d'entreprise est requis dans tous les cas, même si la réunion se tient dans 
le local syndical.

Par personnalités extérieures non syndicales, on entend aussi bien des 
personnalités politiques qu'administratives ou universitaires, ou toute 
personne reconnue dans l'exercice de sa spécialité.

Les sections syndicales des entreprises de 200 salariés au plus qui, sauf 
dispositions conventionnelles, ne disposent que d'un local spécifique, 
devront donc, dans tous les cas, demander l'autorisation de l'employeur avant 
d'inviter des personnalités extérieures, syndicales ou autres.

(1) Par personnalité syndicale, on entend aussi bien le simple militant 
syndical que le délégué syndical d'une entreprise extérieure.

Un crédit d'heures pour la section syndicale ?
===============================================

Dans les entreprises qui occupent au moins 500 salariés, chaque section 
syndicale dispose, au profit de son ou ses délégués syndicaux et des salariés 
de l'entreprise appelés à négocier la convention ou l'accord d'entreprise, 
d'un crédit global spécifique, dans la limite de :

- 10 heures par an dans les entreprises occupant au moins 500 salariés;
- 15 heures par an dans celles occupant au moins 1 000 salariés.

Ces temps de délégation sont de plein droit considérés comme temps de travail 
et payés à l'échéance normale.

Ce crédit s'ajoute à celui dont bénéficient les délégués syndicaux à titre 
individuel.

L'employeur qui contesterait l'usage fait des temps alloués devrait saisir 
le conseil de prud'hommes.

Aménagements des moyens d’action par voie conventionnelle
=========================================================

Les règles posées par le Code du travail et relatives aux sections syndicales 
sont d'ordre public. Une convention ou un accord ne sauraient restreindre les 
droits des syndicats en ce domaine.

Pas question non plus d'apporter une limitation quelconque aux dispositions 
relatives à l'exercice du droit syndical par une note de service ou une 
décision unilatérale de l'employeur (C. trav, art. L. 412-21, al. 2).

En revanche, une convention ou un accord peuvent: aménager les moyens d'action 
des sections syndicales ; comporter des clauses plus favorables aux droits 
des sections syndicales, par exemple :

- permettre la diffusion des documents syndicaux aux heures de cantine et 
  pendant les temps de pause ;
- mettre à la disposition de la section syndicale un local permanent dans les 
  entreprises de moins de 201 salariés;
- autoriser les réunions syndicales pendant les heures de travail ;
- augmenter la périodicité des réunions syndicales dans l'entreprise ;
- prévoir la majoration du crédit d'heures de la section syndicale.

Quels sont les salaries à prendre en compte dans l'effectif ?

Doivent être pris en compte intégralement dans l'effectif de l'entreprise, tous 
les salariés sous contrat à durée indéterminée ; il en est de même :

- des travailleurs à domicile
- des représentants de commerce (mais les VRP multicartes sont assimilés à des 
  travailleurs à temps partiel) ,
- des salariés travaillant à l'étranger, dès lors qu'ils restent sous la 
  subordi­nation de l'employeur en France (Cass. soc. 29janvier 1992, Banque 
  Sudameris c/Syndicat CFDT du personnel des banques) ou qu'ils ont été 
  recrutés en France (Cass. soc. 4 mai 1994, Syndicat CFTC des activités 
  d 'armement c/Sté Cofras et autres),
- des salariés dont le contrat de travail est suspendu ou qui se trouvent en 
  situation de préavis, travaillé ou non;
- des travailleurs handicapés employés dans des entreprises, des ateliers 
  protégés ou des centres de distribution de travail à domicile.

Comment comptabiliser les salariés à temps partiel ?
====================================================

Quelle que soit la nature de leur contrat de travail, ils sont pris en compte 
au prorata de leur temps de travail : on divise la somme totale des horaires 
inscrits dans les contrats de travail par la durée légale du travail ou la 
durée conventionnelle Si celle-ci est inférieure.

Par exemple, 2 salariés travaillent 32 heures par semaine ; ils compteront pour:

2 x 32 : 39 heures = 1,6 salarié.

La durée du travail à prendre en considération est celle figurant sur le contrat 
de travail du salarié, déduction faite des heures complémentaires éventuellement 
prévues.

Quels sont les salariés pris en compte au prorata de leur temps de présence?
=============================================================================

Les salariés sous contrat à durée déterminée, ainsi que les travailleurs mis à 
la disposition de l'entreprise par une entreprise extérieure (y compris les 
travailleurs temporaires) sont pris en compte au prorata de leur temps de 
présence dans l'entreprise, au cours des 12 mois précédents.

Par 12 mois précédents, on entend la période d'un an qui précède le mois pour 
lequel on veut calculer l'effectif. Ainsi, pour calculer les effectifs ,en 
janvier 1997, les salariés de ces deux catégories seront pris en compte s ils 
ont été occupés entre le 1er janvier et le 31 décembre 1996, même Si en 
jan­vier 1997 ils ne font plus partie de l'entreprise.

Exemple : au cours de l'année 1996, une entreprise a eu recours à :

- 3 salariés sous contrat à durée déterminée de 3 mois, d'octobre à décembre;
- 1 salarié sous contrat à durée déterminée de 6 mois, de juillet à décembre;
- 1 salarié sous contrat à durée déterminée de 9 mois, d'avril à décembre.

Ces salariés seront pris en compte pour les effectifs du mois de janvier 1997 
à raison de 2 unités : (3 x 3) + (1 x 6) + (1 x 9) = 24 :12 mois = 2 unités.

Par travailleur mis à disposition, on entend tous les travailleurs présents 
dans l'entreprise mis à la disposition de celle-ci par une autre entreprise, 
que ce soit dans le cadre du travail temporaire ou du prêt de main-d’œuvre 
sans but lucratif.

Toutefois, les salariés sous contrat à durée déterminée ainsi que les 
travailleurs temporaires sont exclus du décompte des effectifs lorsqu'ils 
remplacent un salarié absent ou dont le contrat est suspendu.

Quels sont les salariés à exclure de l'effectif ?
==================================================

Ne doivent pas être pris en compte dans l'effectif:

- les apprentis (C. trav, art. L. 117-11-1);
- les titulaires d'un contrat de qualification;
- les titulaires d'un contrat d'adaptation s'il est à durée déterminée et 
  dans la limite d'une période de deux ans à compter de la conclusion Si le 
  contrat est à durée indéterminée
- les titulaires d'un contrat d'orientation; les titulaires d'un CIE pendant deux ans
- les titulaires d'un contrat emploi solidarité ou d'un contrat emploi consolidé.

Négociation collective dans les entreprises dépourvues de délégués syndicaux
============================================================================

La loi du 12 novembre 1996 (voir Social Pratique n° 246 du i 0 décembre 1996, 
p. 3) reprend les principes retenus par les partenaires sociaux dans leur 
accord national interprofessionnel du 31 octobre 1995 sur la négociation col­lective.

Jusqu'au 31 octobre 1998, toutes les branches ont la possibilité de conclure, 
pour une durée n'excédant pas trois ans, un accord prévoyant que des accords 
collectifs pourront être négociés et conclus dans les entreprises dépourvues 
de délégués syndicaux, ou de délégués du personnel.

Les accords de branche pourront prévoir qu'en l'absence de délégués syndi­caux 
dans l'entreprise, ou de délégués du personnel faisant fonction de délé­gué 
syndical dans les entreprises de moins de cinquante salariés:

Les représentants élus du personnel négocient la mise en œuvre des mesures 
dont l'application est légalement subordonnée à un accord collectif.

Les accords de branche doivent fixer les thèmes ouverts à ce mode de négociation.

Les textes ainsi négociés n'acquièrent la qualité d'accords collectifs de 
travail qu'après leur validation par une commission paritaire de branche, 
prévue par l'accord de branche.

Ils ne peuvent entrer en application qu'après avoir été déposés auprès de 
l'autorité administrative, accompagnés de l'extrait de procès-verbal de la 
commission paritaire compétente.

Des accords collectifs peuvent être conclus par un ou plusieurs salariés 
expressément mandatés, pour une négociation déterminée par une ou plusieurs 
organisations syndicales représentatives.

Les modalités de protection de ces salariés et les conditions d'exercice de 
leur mandat de négociation sont arrêtées par les accords de branche. 
Ces accords peuvent prévoir que le licenciement des salariés mandatés ainsi 
que, pendant un délai qu'ils fixent, le licenciement de ceux dont le mandat 
a expiré sont soumis à la procédure prévue à l'article L. 412-18 du Code du 
travail pour les délégués syndicaux (voir encadré p. 47).

Le seuil d'effectifs en deçà duquel sont applicables les formules dérogatoires 
de négociation est déterminé par les accords de branche.

L'entrée en vigueur des accords de branche est subordonnée à l'absence 
d'opposition de la majorité des organisations syndicales représentatives 
de la branche. L’opposition, qui ne peut émaner que d'organisations non 
signataires desdits accords, devra être notifiée aux signataires dans les 
quinze jours de la signature.

Négociation de la convention ou des accords d'entreprise
========================================================

La convention ou, à défaut, les accords d'entreprise sont négociés entre 
l'employeur et les organisations syndicales de salariés représentatives dans 
l'entreprise.

La délégation de chacune de ces organisations, parties à la négociation, 
comprend obligatoirement le délégué syndical de l'organisation dans 
l'entreprise ou, en cas de pluralité de délégués, au moins deux délégués 
syndicaux (C trav., art. L. 132-19 et L. 132-20).

Un délégué syndical régulièrement désigné pour représenter son organisation 
syndicale auprès du chef d'entreprise est par cette désignation investi de 
plein droit du pouvoir de négocier et de conclure un accord (l'entreprise ; 
et cela quelle que soit l'importance de l'accord (Cass. soc. 19 février 1992, 
SBN - CGC c/Crédit Lyonnais).

Chaque organisation peut compléter sa délégation par des salariés de 
l'entreprise dont le nombre est fixé par accord entre l'employeur et 
l'ensemble des organisations syndicales représentatives. A défaut d'accord, 
ce nombre est au plus égal, par délégation, à celui des délégués syndicaux 
de la délégation. Toutefois, dans les entreprises n'ayant qu'un seul 
délégué syndical, ce nombre peut être porté à deux.

Le temps passé à la négociation est payé comme temps de travail à échéance normale.

Lorsqu'une entreprise emploie, soit dans ses locaux, soit dans un chantier 
dont elle assume la direction en tant qu'entreprise générale des travailleurs 
appartenant à une ou plusieurs entreprises extérieures, les délégués syndicaux 
des organisations représentatives dans ces entreprises sont, à leur demande, 
entendus lors des négociations (C. trav., art. L. 132-21).

Lorsque la convention ou l'accord d'entreprise ou d'établissement comportent 
des clauses dérogatoires, soit à des dispositions législatives ou 
réglementaires (lorsque ces dispositions l'autorisent), soit à des dispositions 
salariales conclues au niveau professionnel ou interprofessionnel, la ou les 
organisations syndicales qui n'ont pas signé l'un des textes en question 
peuvent s'opposer à son entrée en vigueur. A une condition toutefois : 
il faut avoir recueilli les voix de plus de la moitié des électeurs inscrits 
lors des dernières élections au comité ou, à défaut, des délégués du personnel.

Lorsque le texte en cause ne concerne qu'une catégorie professionnelle 
déterminée, relevant d'un collège électoral employés-ouvriers, cadres et 
agents de maîtrise, les organisations susceptibles de s'opposer à son entrée 
en vigueur sont celles qui ont obtenu les voix de plus de la moitié des 
électeurs inscrits dans ce collège.

L'organisation syndicale non signataire doit exprimer son opposition 
par écrit et la motiver dans un délai de huit jours à compter de la signature 
du texte en question (C trav, art. L. 132-26).

L'opposition doit être formée par des personnes mandatées par le ou les 
syndicats n'ayant pas signé l'accord et être notifiée aux organisations 
syndicales signataires (Cass. s oc. 20 mars 1996, Sté Base de Bressols c/Balax).
Négociation annuelle obligatoire

Dans les entreprises où sont constituées une ou plusieurs sections syndicales, 
l'employeur est obligé d'en gager chaque année, avec les organisations 
syndicales représentatives, une négociation sur les salaires effectifs, la 
durée effective et l'organisation du temps de travail (C. trav., art. î  132-27).

La délégation de chacune des organisations syndicales comprend obligatoirement 
le délégué syndical.

Au cours de la première réunion, seront précisées les informations que 
l'em­ployeur remettra aux délégués syndicaux et aux salaries composant la 
délégation, ainsi que le lieu et le calendrier des réunions (C. trav., art. L. 1 3 2- 28).

Négociation du protocole d'accord préélectoral
==============================================

Cf. circulaire DGT n° 20 du 13 novembre 2008

Les règles de négociation du protocole d’accord préélectoral (PAP) sont 
adaptées aux nouvelles règles de représentativité, afin de renforcer la 
légitimité de l’accord trouvé.

En dehors des cas où les règles existantes sont maintenues (v. ci-dessous), 
la validité du protocole est désormais subordonnée à sa signature par la 
majorité des syndicats ayant participé à sa négociation (les syndicats intéressés), 
dont les syndicats représentatifs ayant recueilli la majorité des suffrages 
exprimés lors des dernières élections professionnelles ou, lorsque ces 
résultats ne sont pas disponibles, la majorité des syndicats  représentatifs 
dans l’entreprise. Cette double condition de majorité s’applique pour:

– la répartition du personnel et des sièges entre les collèges DP et CE ;
– la détermination des établissements distincts DP et CE et la perte de cette qualité ;
– les conditions de mise en place des délégués de site ;
– le nombre de membres du CE.

En revanche, les règles existantes sont maintenues dans certains cas.

L’accord unanime des OS représentatives reste exigé pour :
– la suppression du CE ou du mandat de DS ;
– le nombre et la composition des collèges électoraux ;
– l’organisation des élections en dehors du temps de travail ;
– la prorogation des mandats.

Le principe d’un accord avec les OS intéressées est maintenu pour certaines 
dispositions ne nécessitant pas de nouvelles règles de validité, telles que 
les modalités d’organisation et de déroulement des opérations électorales, 
la parité hommes-femmes, etc.

Les règles de validité des accords collectifs de droit commun s’appliquent pour:

– la possibilité de fixer une durée de mandat entre deux et quatre ans ;
– la mise en place du vote électronique ;
– la répartition des compétences CCE/CE en matière d’œuvres sociales.

Mise en œuvre du droit d'expression
===================================

Dans les entreprises d'au moins 50 salariés disposant d'au moins un délégué 
syndical, les modalités d'exercice du droit à l'expression sont définies par
un accord conclu entre l'employeur et les organisations syndicales 
représentatives dans l'entreprise.

En cas d'absence d'accord, l'employeur est tenu d'engager une fois par an une 
négociation en vue de conclure cet accord.

Si l'accord prévu a été effectivement conclu, l'employeur doit, au moins une 
fois tous les trois ans, tenir une réunion avec les syndicats représentatifs 
pour examiner les résultats de l'accord et engager la renégociation de cet 
accord, a la demande d'une organisation syndicale. En l'absence d'initiative 
de la part de l'employeur, la négociation s'engage obligatoirement à la 
demande d'une organisation syndicale représentative dans les quinze jours 
suivant la présentation de cette demande.

Autres interventions des délégués syndicaux
============================================

Travail de fin de semaine
------------------------- 

L'autorisation de faire travailler une deuxième é
quipe en fin de semaine, par dérogation a la règle générale du repos dominical 
est, à défaut d'accord d'entreprise ou d'établissement, donnée par l'ins­pecteur 
du travail, après consultation des délégués syndicaux et avis du comi­té 
d'entreprise, ou des délégués du personnel s'ils existent 
(C. trav, art. L 221-5-] etR. 221-15).

Travail à temps partiel
-----------------------

L'employeur doit communiquer au moins une fois par an aux délégués syndicaux 
un bilan du travail à temps partiel réalisé dans l'entreprise, portant notamment 
sur le nombre, le sexe et la qualification des salariés concernés ainsi que 
les horaires de travail à temps partiel pratiqués et le nombre de contrats 
de travail a temps partiel ouvrant droit à l'abattement de cotisations sociales 
(C. trav., art. L. 212-4-5).

Égalité professionnelle des hommes et des femmes. Les délégués syndicaux 
reçoivent communication du rapport annuel de l'employeur sur la situation 
comparée des conditions générales d'emploi et de formation des femmes et 
des hommes dans l'entreprise, dans les mêmes conditions que le comité 
d'entreprise (C. trav, art. L. 432-3-]).

Les délégués syndicaux se voient également communiquer au moins une fois 
par an, les documents d'information remis au CE pour l'élaboration du plan de 
formation (C. trav, art. L. 933-3, ai. 6) et ils reçoivent le projet de 
bilan social dans les mêmes conditions que les membres des comités d'entreprise 
ou d'établissement (C. trav, art. L. 438-5) (1oirpagc 56).
