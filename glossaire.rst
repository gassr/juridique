

.. index::
   ! glossaire

.. _glossaire_juridique:
.. _ref_glossairejuridique:

===================
Glossaire Juridique
===================

.. glossary::
   :sorted:

   ANI
       Accord National Interprofessionnel

       .. seealso:: :ref:`ani`

   Comité d'Etablissement
   C.E
   CE
      Comité d'Etablissement.

      .. seealso:: https://fr.wikipedia.org/wiki/Comit%C3%A9_d%27%C3%A9tablissement


   DéléguéE du personnel
   D.P
   DP
      DéléguéE du personnel.
      Les déléguéEs du personnel sont une institution représentative des
      salariés en France, créée en 1936 par le Front populaire.
      Supprimés sous le régime de Vichy, ils ont été rétablis par la loi du
      16 avril 1946.
      La participation aux élections professionnelles (63,8% en moyenne) est
      un baromètre de représentativité qui permet aux délégués du personnel
      de peser dans les relations avec l'employeur.

      .. seealso:: https://fr.wikipedia.org/wiki/D%C3%A9l%C3%A9gu%C3%A9_du_personnel_en_France


   Décret
       Un décret (du latin decretum, « décision ») est une décision émise par
       une autorité souveraine.
       Actuellement, en France, un décret est une norme émanant du pouvoir
       réglementaire. Il est pris par le Premier ministre, éventuellement
       contresigné par les ministres concernés par son application, ou par le
       président de la République.
       Dans la hiérarchie des normes, il prend une valeur supérieure aux arrêtés.

       .. seealso::  https://fr.wikipedia.org/wiki/D%C3%A9cret

   DéléguéE syndical
   D.S
   DS
      DéléguéE syndical.
      Dans une entreprise ou un établissement de plus de 50 salariés,
      le Délégué syndical est un salarié désigné par un syndicat représentatif,
      qui a pour fonction de représenter cette organisation et de négocier
      des accords collectifs.
      Dans les structures de plus petite taille, un délégué du personnel peut
      être désigné délégué syndical. Il bénéficie d'une protection
      administrative contre le licenciement.

      .. seealso::

         - https://fr.wikipedia.org/wiki/D%C3%A9l%C3%A9gu%C3%A9_syndical


   DUP
   Délégué Unique du Personnel
       Délégué Unique du Personnel

       La délégation unique du personnel (DUP) peut, en France depuis la
       "loi Rebsamen" (L. no 2015-994 JO 18 août) du 17 août 2015, remplacer
       et regrouper les institutions représentatives du personnel que sont le
       Comité d'entreprise,les délégués du personnel et le Comité d'Hygiène
       de Sécurité et des Conditions de Travail (CHSCT) dans certaines
       conditions : entreprise de 50 à moins de 300 salariés et consultation
       des représentants éventuellement déjà élus.
       Cette possibilité est ouverte à l'occasion de la mise en place d'un
       comité d'entreprise ou lors de son renouvellement par le biais
       d'élections professionnelles.


       .. seealso::

          - https://fr.wikipedia.org/wiki/D%C3%A9l%C3%A9gation_unique_du_personnel

   Représentant Syndical de Section
   R.S.S
   RSS
      Représentant Syndical de Section.

      .. seealso::

         - :ref:`L2142_1_1`
         - http://www.cnt-f.org/59-62/archives/syndic.htm


   Secrétariat Juridique
       Secrétariat Juridique créé lors du Congrès de Saint-Etienne en 2010.

       .. seealso::  comm.juridique.conf@cnt-f.org


   Section Syndicale d'Entreprise
   S.S.E
   SSE
       Section Syndicale d'Entreprise.

       Depuis la loi du 20/08/2008 une section syndicale d'entreprise peut être
       créée dans n'importe quelle entreprise par des syndicats qui possèdent
       au moins 2 salariés encartés dans l'entreprise ou l'établissement
       (arrêt de la chambre sociale du 8 juillet 2009 dit "Okaidi")

       .. seealso::

          - https://fr.wikipedia.org/wiki/Section_syndicale_d%27entreprise
          - http://www.cnt-f.org/59-62/syndic.htm


