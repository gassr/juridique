
.. index::
   pair: Gérard; Filoche


.. _gerard_filoche:

=============================
Gérard Filoche 
=============================

.. seealso:: 

   - http://www.filoche.net/
   - http://twitter.com/#!/gerardfiloche
   

.. contents::
   :depth: 3
   

Twitter
=======

:twitter: http://twitter.com/#!/gerardfiloche
   

.. toctree::
   :maxdepth: 3
   
   2012/index
   
   
