
.. index::
   pair: Juridique; Rémunérations

.. _juridique_remunerations:

=============
Rémunerations
=============

.. figure:: repartition_egalitaire_des_richesses.jpg
   :align: center
   
   :ref:`campagne_confederale_repartition_egalitaire_des_richesses`

.. figure:: equality_trust.png
   :align: center

   http://www.equalitytrust.org.uk/   
   
.. toctree::
   :maxdepth: 4
   
   smic/index
