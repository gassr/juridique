
.. index::
   pair: Juridique; SMIC
   ! SMIC

.. _smic:

=============
SMIC
=============

.. seealso::
 
   - http://www.travail-emploi-sante.gouv.fr/informations-pratiques,89/fiches-pratiques,91/remuneration,113/le-smic,1027.html
   - https://fr.wikipedia.org/wiki/SMIC

.. figure:: ../repartition_egalitaire_des_richesses.jpg
   :align: center
   
   :ref:`campagne_confederale_repartition_egalitaire_des_richesses`



.. figure:: ../equality_trust.png
   :align: center

   http://www.equalitytrust.org.uk/

Le Salaire minimum interprofessionnel de croissance1, plus connu sous 
l'acronyme SMIC, anciennement Salaire minimum interprofessionnel garanti 
(SMIG), est, en France, le salaire minimum horaire en dessous duquel aucun 
salarié ne peut être payé. 

Il est réévalué au minimum tous les ans le 1er janvier.

À la différence du SMIG basé sur l'inflation, le SMIC est revalorisé au 
minimum à hauteur de la moitié de l’augmentation du pouvoir d’achat du salaire 
horaire de base ouvrier (SHBO). 

Il peut bénéficier de coup de pouce de la part  du gouvernement.

.. toctree::
   :maxdepth: 4
   
   2012/index
