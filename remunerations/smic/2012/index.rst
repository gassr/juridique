

.. _smic_2012:

=============
SMIC 2012
=============


Depuis le 1er janvier 2012, la valeur du SMIC brut est de 9,22 € par heure 
soit 1 398.37 € brut mensuels la durée légale de 35 heures hebdomadaires.
