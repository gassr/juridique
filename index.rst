.. index::
   ! Juridique


.. figure:: images/bandoconf.jpg
   :align: center


.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center


.. _juridique_pub:
.. _cnt_juridique:

====================================================================
**CNT Juridique**
====================================================================

- comm.juridique.conf@cnt-f.org
- :ref:`glossaire_juridique`
- http://www.cnt-f.org/spip.php?rubrique3 (vos droits)


:Adresse courriel: comm.juridique.conf@cnt-f.org


.. figure:: images/tm28couverture.jpg
   :align: center
   :width: 300

   *Le droit et les travers TM n°28*


.. toctree::
   :maxdepth: 9

   people/people
   experiences/experiences
   section_syndicale_entreprise/section_syndicale_entreprise
   code_juridique/index
   conventions_collectives/conventions_collectives
   cour_cassation/cour_cassation
   decrets/index
   droit/droit
   commission_europeenne/commission_europeenne
   livres/livres
   remunerations/remunerations
   violence_au_travail/violence_au_travail
   glossaire
   meta/meta

