
.. index::
   pair: Convention collective ; Métallurgie 3025

.. _convention_collective_metallurgie_3025:

===========================================================
Convention collective métallurgie ingénieurs et cadres 3025 
===========================================================

.. seealso:: 

   - http://www.juritravail.com/convention-collective/brochure-3025/metallurgie-ingenieurs-et-cadres.html
   - :ref:`convention_collective_metallurgie_3126`

.. image:: achat_uimm_3025.png


La convention collective des ingénieurs et cadres de la métallurgie aussi 
appelée convention collective 3025 ou UIMM convention collective a pour objet 
de donner à cette catégorie socio-professionnelle les garanties en rapport 
avec le rôle qu'ils assument dans les entreprises et de leur assurer le 
maintien d'une hiérarchie correspondant à ce rôle. Elle s'applique au personnel 
des entreprises situées en territoire métropolitain. Elle dresse également les 
critères d'éligibilité des salariés,relatifs notamment aux diplômes requis et 
à l'expérience professionnelle. Sont exclus de son champ d'application les VRP.
 
En relation la convention collective de la métallurgie accords nationaux ainsi 
que la convention 3126 qui correspond à la convention métallurgie région parisienne.

