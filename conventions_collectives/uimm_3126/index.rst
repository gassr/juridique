
.. index::
   pair: Convention collective ; Métallurgie 3126

.. _convention_collective_metallurgie_3126:

===========================================================
Convention collective métallurgie Paris 3126
===========================================================

.. seealso:: 

   - http://www.juritravail.com/convention-collective/brochure-3126/metallurgie-region-parisienne.html
   - :ref:`convention_collective_metallurgie_3025`

La convention collective de la métallurgie région parisienne aussi appelée 
convention 3126 ou UIMM convention collective  règle les rapports entre 
employeurs et salariés  des industries métallurgiques, mécaniques connexes 
et similaires situées en région parisienne, à savoir les départements 
de Paris, de la Seine-Saint-Denis, des Hauts-de-Seine, du Val-de-Marne, 
des Yvelines, du Val-d'Oise, de l'Essonne. 

Les voyageurs, représentants et placiers ne peuvent se prévaloir des avenants 
de la convention mais seulement de ses dispositions générales. 

Par ailleurs,  les ingénieurs et cadres, régis par la convention collective 
métallurgie des ingénieurs et cadres (brochure JO n°3025) sont également exclus. 


