
.. index::
   ! Conventions collectives

.. _conventions_collectives:

=======================
Conventions collectives
=======================

- https://www.juritravail.com/convention-collective.html
- https://fr.wikipedia.org/wiki/Conventions_collectives

Introduction
============

Dans certaines législations (droit du travail), une convention collective de
travail (CCT) est un texte réglementaire définissant chacun des statuts des
employés d'une branche professionnelle, après une négociation passée entre les
organisations représentant les employeurs et les organisations représentant les
salariés (syndicat).

Les contrats de travail doivent s'y référer en précisant le type d'emploi et
le coefficient de rémunération.

Pour une activité et un territoire donnés (on trouve parfois des conventions
au niveau national (CCN)).

Lorsqu'il s'agit d'une convention collective étendue, elle prend force de loi.

En droit Français
=================

En France, les conventions collectives sont des décrets pris en Conseil d’État,
comme les statuts de la fonction publique, publiés au Journal officiel.

Elles viennent compléter les dispositions du :ref:`Code du travail <code_du_travail>`
pour chaque branche de métier et pour chaque profession, y compris les
professions libérales réglementées comme les architectes ou les avocats.

En cas de contradiction entre le Code du travail et la convention collective,
ce sont les dispositions les plus favorables au salarié qui sont applicables
(article L. 2251-1 du Code du travail français. Il s’agit du principe de faveur.

En revanche, généralement parlant (il y a des exceptions comme SYNTEC), une
convention collective ne peut comporter de clauses moins favorables au salarié
que le Code du travail.

.. warning:: il y a des exceptions comme l'écoeurante convention SYNTEC !

La convention collective définit les règles suivant lesquelles s’exerce le
droit des salariés à la négociation collective de l’ensemble de leurs
conditions d’emploi, de formation professionnelle, de travail et de leurs
garanties sociales. Il peut exister des accords collectifs qui eux ne
définissent qu’une partie des points ci-dessus.

Une convention collective doit nécessairement faire l’objet d’un écrit (à peine
de nullité) et d’un dépôt de l’accord à la DIRECCTE ainsi qu’au secrétariat du
greffe du conseil de prud’hommes.

Une convention collective est relative à un type d’activité.

Elle peut aussi être nationale ou restreinte à une région, à un département,
voire propre à une entreprise ou à un établissement.

Il existe en effet quatre niveaux de négociations collectives, qui sont les
suivantes:

- niveau national interprofessionnel : négocié par les syndicats représentatifs
  au niveau national (toute réforme concernant le droit du travail, de l’emploi
  et de la formation doit faire l’objet d’une concertation entre employeurs et
  syndicats de salariés) ;
- niveau de la branche : négocié entre les syndicats représentatifs de la
  branche d’activité et les employeurs ;
- niveau du groupe : négocié entre les syndicats représentatifs du groupe
  (groupement d’entreprises) et l’employeur de l’entreprise dominante (dans le
  cas du régime des sociétés mères et filiales c’est donc l’employeur de la
  société mère qui négocie la convention) ;
- niveau de l’entreprise ou de l’établissement : négocié entre le syndicat
  représentatif dans l’entreprise ou l’établissement et le chef d'entreprise.


Initialement, les conventions collectives ne concernaient que les entreprises
signataires. La plupart des conventions collectives ont été étendues à toutes
les entreprises de la branche concernée par arrêté du ministre chargé du Travail.

La quasi-totalité des entreprises françaises relèvent donc d’une convention
collective quelles que soient leur activité et leur taille.

**Le contrat de travail doit obligatoirement mentionner la convention collective
dont relève l’entreprise**, et un exemplaire doit être disponible pour
consultation dans l’entreprise par les salariés.

Il existe une commission nationale de la négociation collective, formée par
les ministres chargés de l’Emploi, de l’Agriculture et de l’Économie ainsi
que par des représentants des syndicats nationaux. Cette commission est chargée
entre autres d’examiner l’évolution des lois régissant les conventions
collectives ainsi que de donner un avis sur les arrêtés d’extension ou
d’élargissement des conventions collectives.

La création des conventions collectives a été adoptée le 25 mars 1919, mais
elles seront réellement appliquées à partir du Front populaire, en 1936.


Conventions collectives
=======================

.. toctree::
   :maxdepth: 4

   uimm_3025/index
   uimm_3126/index

