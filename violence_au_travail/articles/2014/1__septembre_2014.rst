
   
.. _articles_violence_au_travail_septembre_2014:

==================================
Articles Violence au travail 2014
==================================



> j'ai regardé le document envoyé par Guillaume (http://www.samuel-bollendorff.com/fr/le-grand-incendie-teaser/ )
> vous connaissez tous certainement Christophe Dejours, professeur à la 
> chaire de psychanalyse santé-travail au CNAM

non je ne connaissais pas :)

- http://www.souffrance-et-travail.com/rubriques/video/conferences/christophe-dejours/

> un de ses ouvrages : " souffrance en France, la banalisation de 
> l'injustice sociale " et des ravages engendrés par la casse du collectif...
>
> avez-vous d'autres références ? merci

Sur Grenoble, il y a Brigitte Font Le Bret qui participe régulièrement à 
des réunions sur Grenoble.
Elle a écrit un livre sur le harcèlement moral "Pendant qu'ils comptent 
les morts"


- http://blogs.mediapart.fr/blog/brigitte-font-le-bret
- "Pendant qu'ils comptent les morts" http://www.bibliothequeantigone.org/index.php?option=com_eventlist&view=day&id=20100610&Itemid=48
- http://www.sante-et-travail.fr/brigitte-font-le-bret--psychiatre-du-travail_fr_art_921_48887.html
- http://boutique.la-tengo.com/no-fiction/10-pendant-qu-ils-comptent-les-morts.html

Elle est intervenue à la bibliothèque Kateb Yacine le samedi 25 mai 2013 
après le visionnage du film "Harcelé à perdre la raison"  
(http://www.souffrance-et-travail.com/video/harcele-a-en-perdre-la-raison/)

Autres liens
============

- http://fr.wikipedia.org/wiki/Harcèlement_moral
- http://www.souffrance-et-travail.com/
- http://www.souffrance-et-travail.com/a-propos/le-site/
- http://www.souffrance-et-travail.com/liens/
- https://www.facebook.com/SouffranceEtTravail


  
