
.. index::
   pair: Brigitte; Font-Le_bret

.. _brigitte_font_le_bret:
.. _brigitte_font_lebret:

=====================
Brigitte Font Le bret 
=====================

.. seealso::

   - http://blogs.mediapart.fr/blog/brigitte-font-le-bret


.. figure:: brigitte_font_lebret.jpeg
   :align: center
   
   Brigitte Font-Le-bret 



Coordonnées Corenc
==================

:Téléphone: 04.76.22.24.97
:Portable: 06 84 33 91 59
:Adresse courriel: dr.font-le-bret@wanadoo.fr

    Brigitte Font-Le-bret
    34 avenue Marius Cottier 
    38700 Corenc 


Coordonnées Hopital
====================

Adresse::

    Hôpital de La Tronche - CHU Grenoble
    Avenue du Maquis du Grésivaudan
    38700 La Tronche 
    
    
:Téléphone: 04.76.76.75.75 

Activités
=========


.. toctree::
   :maxdepth: 3
   
   2013/index
   2012/index
   2011/index
   2010/index
   2009/index      
   

   
