
.. index::
   pair: Harèlement; Moral

.. _harcelement_moral:

===================
Harcèlement moral 
===================

.. seealso:: 

   - http://fr.wikipedia.org/wiki/Harc%C3%A8lement_moral


.. contents::
   :depth: 3
   
Description
============

Le harcèlement moral est une conduite abusive qui par des gestes, paroles, 
comportements, attitudes répétés ou systématiques vise à dégrader les 
conditions de vie et/ou conditions de travail d'une personnes (la victime du 
harceleur).

Ces pratiques peuvent causer des troubles psychiques ou physiques mettant en 
danger la santé de la victime (homme ou femme).

Le «harcèlement moral» est une **technique de destruction** ; Il n'est pas un 
syndrome clinique.

   
Articles
========

.. toctree::
   :maxdepth: 3
   
   articles/index
   
      
