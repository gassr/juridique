.. index::
   ! meta-infos


.. _juridique_meta_infos:

=====================
Meta
=====================

.. seealso::

   - https://framagit.org/cntvignoles/meta


.. contents::
   :depth: 3


Gitlab project
================

.. seealso::

   - https://framagit.org/france1/juridique


Issues
--------

.. seealso::

   - https://framagit.org/france1/juridique/-/boards

Pipelines
-----------

.. seealso::

   - https://framagit.org/france1/juridique/-/pipelines


Sphinx theme : **sphinx_book_theme**
============================================

.. seealso::

   - https://gdevops.gitlab.io/tuto_documentation/doc_generators/sphinx/themes/sphinx_book_theme/sphinx_book_theme.html


::

    liste_full = [
        "globaltoc.html",
    html_theme = "sphinx_book_theme"
    copyright = f"2011-{now.year}, assr38, Creative Commons CC BY-NC-SA 3.0. Built with sphinx {sphinx.__version__} Python {platform.python_version()} {html_theme=}"



root directory
===============

::

    $ ls -als

::

    4 drwxr-xr-x 20   4096 nov.  23 14:24 .
    4 drwxr-xr-x  3   4096 nov.  23 14:04 ..
    4 drwxr-xr-x  4   4096 nov.  23 14:21 _build
    4 drwxr-xr-x  3   4096 nov.  23 14:04 code_juridique
    4 drwxr-xr-x  3   4096 nov.  23 14:04 commission_europeenne
    4 -rwxr-xr-x  1   3676 nov.  23 14:20 conf.py
    4 drwxr-xr-x  4   4096 nov.  23 14:04 conventions_collectives
    4 drwxr-xr-x  3   4096 nov.  23 14:04 cour_cassation
    4 drwxr-xr-x  3   4096 nov.  23 14:04 decrets
    4 drwxr-xr-x  5   4096 nov.  23 14:11 droit
    4 drwxr-xr-x  4   4096 nov.  23 14:04 experiences
    4 -rw-r--r--  1    110 oct.  27 18:12 feed.xml
    4 drwxr-xr-x  8   4096 nov.  23 14:24 .git
    4 -rwxr-xr-x  1     49 nov.  23 14:04 .gitignore
    4 -rw-rw-rw-  1    214 août  19 17:43 .gitlab-ci.yml
    8 -rwxr-xr-x  1   4392 nov.  23 14:04 glossaire.rst
    4 drwxr-xr-x  2   4096 nov.  23 14:04 images
    4 drwxr-xr-x  2   4096 oct.  19 18:09 index
    4 -rwxr-xr-x  1   1256 nov.  23 14:21 index.rst
    4 drwxr-xr-x  3   4096 nov.  23 14:04 livres
    4 -rw-r--r--  1   1154 mars  30  2020 Makefile
    4 drwxr-xr-x  2   4096 nov.  23 14:24 meta
    4 drwxr-xr-x  3   4096 nov.  23 14:04 people
    84 -rw-r--r--  1  84904 nov.  23 14:17 poetry.lock
    4 -rw-rw-rw-  1   1141 oct.  26 17:22 .pre-commit-config.yaml
    4 -rw-rw-rw-  1    338 oct.  26 17:44 pyproject.toml
    4 drwxr-xr-x  3   4096 nov.  23 14:04 remunerations
    4 -rw-r--r--  1   2060 nov.  23 14:18 requirements.txt
    4 drwxr-xr-x  5   4096 nov.  23 14:04 section_syndicale_entreprise
    4 drwxr-xr-x  2   4096 nov.  23 14:04 sites_juridiques
    56 -rwxr-xr-x  1  53336 nov.  23 14:04 tm28couverture.jpg
    4 drwxr-xr-x  7   4096 nov.  23 14:04 violence_au_travail




pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
