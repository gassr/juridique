


============================================================================
Expérience SUB-RP
============================================================================

La commission confédérale sur la question du syndicat du nettoyage, à laquelle 
notre syndicat participe, a invité les différents syndicats de la CNT à 
témoigner sur leur pratique juridique et les moyens mis en œuvre à cet effet.

C’est dans ce cadre que le SUB RP, produit ce texte.

::

	Que faire contre le patron ?
	Il n’y a pas à hésiter : le forcer à respecter la loi. 

Emile Pouget – La Voix du Peuple – 6/13 avril 1902


Le SUB RP est engagé depuis sa réactivation (1997), dans la défense des 
intérêts des travailleurs et utilise le recours au juridisme pour faire 
reconnaître leurs droits. Sa composition (adhérents travaillant majoritairement 
dans des entreprises de moins de 20 salariés, ou en missions d’intérim) l’a 
amené plus souvent devant les Conseils de prud’hommes que devant les TGI.

Lors de la vague de syndicalisation de travailleurs sans ‐papiers, il a acquis 
d’autres connaissances concernant le droit des étrangers.

Le syndicat traite directement et simultanément une vingtaine de dossiers 
prud’hommes, auxquels s’ajoute une dizaine d’autres traités par un avocat mais 
dont le syndicat suit l’évolution. Et suit les procédures engagées pour la 
régularisation d’une quinzaine de travailleurs sans ‐papiers.

le syndicalisme a l’epreuve du droit
=====================================

Le plus radical des syndicats est bien obligé, sous peine de renoncer à sa 
fonction de défense des intérêts ouvriers, d’entretenir avec la partie adverse, 
le patronat, et plus généralement avec la société environnante, des rapports 
constants qui supposent négociation et compromis. La revendication juridique 
n’est pas un phénomène récent, elle ne date pas de la dégénérescence du 
syndicalisme révolutionnaire, mais est née avec le syndicalisme lui‐même. 

Durant la phase de formation de la CGT, il n’est pas de sujet de droit
social où les syndicalistes n’interviennent pas, ne refusant aucun moyen 
(presse, participation à des organismes étatiques, création de conseils 
juridiques syndicaux, interpellation de parlementaires, grève,
boycott et sabotage) pour faire avancer la conscience de classe.

**Notre pratique du juridique, si elle s’exprime dans le cadre de la loi et de 
ses espaces, n’en doit demeurer pas moins pour nous une pratique d’action directe.**

Nous pensons que ce n’est pas la révolution qui est émancipatrice en elle ‐même, 
car l’histoire nous l’a prouvé. La révolution peut aussi bien faire œuvre 
d’asservissement. C’est l’action directe qui est émancipatrice, soit qu’elle 
aboutisse à des réformes partielles, soit qu’elle aboutisse à la révolution.

« Le jour n’est pas loin où tous les militants reconnaîtront que la véritable 
action révolutionnaire est celle qui, pratiquée chaque jour, accroît et 
augmente la valeur révolutionnaire du prolétariat. »
Victor Griffuehles – Le Mouvement Socialiste » ‐ Janvier 1905

A défaut de reprendre la prédiction ("le jour n’est pas loin") de Griffuelhes 
à notre compte, nous ne pouvons que réaffirmer que les outils actuels de 
défense du droit des travailleurs doivent être ceux de notre émancipation 
future, et qu’à défaut nous n’allons pas vers la révolution, mais au renforcement 
de la collaboration de classe comme y travaille le « syndicalisme de service ».

Pratique juridique (principalement prud’homale) au sein du SUB RP
=================================================================

Le syndicat considère:

- Que la lutte se gagne dans l’entreprise, par la mobilisation des salariés, 
  et non au Prud’hommes, espace de paritarisme et de gestion individuelle des 
  conflits.
- Que le syndicat se doit de répondre aux sollicitations même individuelles des 
  travailleurs et tenter de renforcer la conscience de classe chez ces derniers, 
  et que le temps d’instruction de ces affaires peut le permettre.
- Qu’à défaut d’une lutte collective la réponse syndicale doit l’être (gestion 
  collective et formatrice du dossier)
- Qu’en aucun cas le service de défense juridique du syndicat ne doit prendre 
  le pas sur les autres activités du syndicat, ni entrainer la spécialisation 
  de certains des camarades.
  
Fonctionnement
--------------

Tout salarié de notre industrie se présentant à l’une de nos permanences est 
reçu par un camarade qui écoute sa demande et lui propose un rendez‐vous 
(immédiat où sous huit jours) suivant l’urgence de la question, pour analyser 
le problème et envisager la ou les actions possibles. Si l’urgence le réclame, 
le syndicat peut immédiatement aider le salarié dans la rédaction d’un courrier.

Après l’analyse de la situation le syndicat donne conseil au travailleur sur 
ce qui est envisageable. En fonction des actions possibles le syndicat propose 
l’adhésion ou indique au salarié les autres acteurs possibles
(avocats, services sociaux, inspection du travail, etc). 

Pour la défense, le syndicat peut agir de manières différentes (Décision du 
Conseil Syndical du 10/09/2009). 

Dossiers montés et plaidés par le syndicat
------------------------------------------

- Toute demande faite, pour lui‐même, par un adhérent ayant plus de 6 mois de 
  cotisation et jouissant de ses droits syndicaux.
- Tous travailleurs amenés par des adhérents (en tendant vers l’implication 
  de « l’adhérent parrain », sur le principe de l’accompagnement syndical)
- Nouvel adhérent en conflit avec son employeur mais toujours salarié dans 
  une entreprise de plus de 10 salariés
- Nouvel adhérent, licencié dans une entreprise de plus de 5 salariés et ayant 
  plus de 2 ans d’ancienneté
- Nouveaux adhérents venus collectivement et issus d’une même entreprise.


Renvoi vers avocat
==================

- Travailleur (non adhérent), licencié d’une entreprise de moins de 5 salariés
- Travailleur (non adhérent), licencié et ayant moins de deux ans d’ancienneté

Dossiers montés mais non plaidés par le syndicat
-------------------------------------------------

- Nouvel adhérent travailleur non déclaré dans une boite de moins de 10 salariés
- Dossier clairement perdu

Engagement de l’adhérent
------------------------

Les adhérents qui demandent l’aide du syndicat dans une affaire prud’homale 
sont informés que la perte de leurs droits syndicaux durant cette période 
peut entrainer le désengagement du syndicat. Ils sont d’autre part d’accord 
pour donner 10% de ce qui leurs sera versé au syndicat.

Les droits syndicaux 
--------------------

Tout adhérent dispose des mêmes droits à la solidarité syndicale s’il respecte 
ses devoirs syndicaux (cotisations a jour, présence aux AG du syndicat 
(au nombre d’une par trimestre), participation aux actions décidées en AG).

‐ Une absence provoque un rappel,
‐ Deux absences une suspension temporaire des droits syndicaux,
‐ Trois absences consécutives une radiation.

Engagement du syndicat
======================

Le syndicat garantie le montant de l’article 700 si l’affaire est perdu et la 
somme réclamée par la partie adverse.

Traitement Syndical
-------------------

Les permanences et le suivi des dossiers juridiques est assuré par le Conseil 
Syndical composé actuellement de 12 membres. Le constituent les camarades 
porteurs de mandats internes (fonctionnement du syndicat) et externes (RSS) 
ainsi que les membres des coopératives syndicales.

L’ensemble de ces camarades doivent progressivement être en capacité de:

‐ Recevoir et orienter un travailleur venant dans une de nos permanences
‐ Analyser un conflit du travail
‐ Remplir une saisine prud’homale
‐ Constituer un dossier Prud’hommes
‐ Rédiger des conclusions
‐ Plaider le dossier devant le Conseil des Prud’hommes.

Ces compétences s’acquièrent:

‐ Dans la pratique des permanences
‐ Dans la participation aux réunions (hebdomadaires) du Conseil Syndical
‐ Lors des formations mensuelles (obligatoires pour les membres du Conseil Syndical)
‐ Par l’accompagnement du montage de dossiers avec des camarades plus expérimentés.


Difficultés rencontrées
-----------------------

‐ L’handicap scolaire de nombreux camarades manuels
‐ La barrière psychologique d’un handicap scolaire (réel ou supposé)
‐ La disponibilité des salariés pour aller plaider (pendant le temps de travail)

Contournement 
--------------

‐ Rappel du principe de procédure orale
‐ Familiarisation avec le droit du travail (de base)
‐ Familiarisation avec les outils (pratique de jeu de rôles pendant les formations)
‐ Formation de Conseillers du salarié et présentation de listes (à venir)


La plus grande difficulté concerne l’implication effective du salarié défendu 
dans le montage de son dossier allant:

De la recherche des pièces et témoignages

A la réalisation des tableaux justificatifs

L’implication dans la relecture des conclusions

La mobilisation même durant l’affaire certains adhérents disparaissent entre 
deux convocations devant le Conseil des Prud’hommes, donnant seulement signe 
de vie une semaine avant le jugement.


le syndicalisme d’action directe peut‐il se dissoudre dans le juridisme ?
==========================================================================

Si la gestion syndicale de ce type de dossier peut être et doit être collective, 
cette action individuelle peut‐elle être une action directe au sens où la 
définissait Griffuelhes dans la revue "le Mouvement Socialiste" en janvier 1905 ?

« Action directe veut dire action des ouvriers eux ‐mêmes, c’est ‐à ‐dire 
action directement exercée par les intéressés. C’est le travailleur qui 
accomplit lui ‐même son effort ; il exerce personnellement sur les puissances 
qui le dominent, pour obtenir d’elles les avantages réclamés. 

Par l’action directe, l’ouvrier créé lui‐même sa lutte ; c’est lui qui la 
conduit, décidé à ne pas s’en rapporter à d’autres qu’à lui ‐ même du soin de 
le libérer. »

Le SUB RP ne peut répondre aujourd’hui, au regard de sa simple expérience, que 
par la négative.

Si cette démarche est formatrice pour les militants du syndicat et 
particulièrement pour les membres de son Conseil Syndical. 

Si sa prise en charge collective est une action directe qui demeure le principe 
de base de notre syndicalisme de transformation sociale. 

La démarche qui motive le salarié qui vient nous voir sur ce type de demande 
reste principalement personnelle.

En ce sens notre syndicat s’implique dans cette action parce que:

Elle concerne 60% des contacts que notre syndicat reçoit aujourd’hui 
(30 autres % concernent des questions de régularisation, les 10% restants 
sont des contacts idéologiques)

L’organisation de la profession est éclatée et composée d’une multitude de 
très petites entreprises où les salariés sont isolés et donc peu en contact 
avec des organisations syndicales (même réformistes)

Les Chambres d’industries des Conseils de Prud’hommes sont largement occupées 
par des conflits concernant le BTP

Elle est formatrice de nos militants sur le droit du travail


Le syndicat oeuvre, le temps de l’instruction de l’affaire, a réveiller chez 
le salarié demandeur une conscience collective et syndicale qui le fera 
rester adhérent (et peut être militant) de l’organisation après le jugement.
  

.. warning:: C’est pourquoi, considérant que le réel développement du syndicat 
   passe par une  lutte syndicale offensive au sein des entreprises, le syndicat 
   porte les plus  grandes réserves sur une tendance à la juridiciarisation 
   syndicale amenant la  constitution d’une caste technicienne en son sein et 
   la professionnalisation interne de cette fonction dans le syndicat.


::

	Fraternelles Salutations Syndicalistes
	SUB TP BAM de la Région Parisienne



