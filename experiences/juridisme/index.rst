


============================================================================
Le juridisme est‐il possible dans une pratique syndicale d’action directe ?
============================================================================

**Nous abhorrons les lois, c’est entendu. Devons nous pour cela négliger de les 
connaître ?**

Lorsque nous en sommes victimes (et c’est souvent, et c’est toujours) 
devons-nous rejeter tout ce qui peut quelques fois nous tirer des griffes de ce 
monstre qui mange les petits et caresse les gros, qu’on appelle la justice ?
(...)

Non, n’est‐ce pas ? Eh bien, servons nous donc de ce qui s’offre gracieusement 
à nous. Nous voulons continuer et connaître mieux encore la législation et ses 
finesses afin de mieux combattre les lois et afin de mieux préserver ceux qui 
peuvent en être victimes.**

Georges Yvetot, congrès des Bourses – Nice 1901

