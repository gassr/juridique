
.. index::
   pair: Code du travail; Les relations collectives de travail



.. _code_du_travail_P2:

======================================================================
Deuxième partie: les relations collectives de travail
======================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/167521-les-relations-collectives-de-travail
   

.. toctree::
   :maxdepth: 4
   
   livre_premier/index
   livre_deux/index
