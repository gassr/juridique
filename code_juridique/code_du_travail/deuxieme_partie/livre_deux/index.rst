
.. index::
   pair: Code du travail; Livre deux; la négociation collective 
   pair: Code du travail; Livre deux; les conventions et accords collectifs de travail 


.. _code_du_travail_P2_L2:

========================================================================================
Livre deux: la négociation collective - les conventions et accords collectifs de travail
========================================================================================

.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/167523-la-negociation-collective-les-conventions-et-accords-collectifs-de-travail
   - :ref:`code_du_travail_P2`
   
.. toctree::
   :maxdepth: 4
   

   titre5/index
