
.. index::
   pair: Code du travail; Rapports entre conventions ou accords et lois et règlements


.. _code_du_travail_P2_L2_T5_CH1:

========================================================================================
Chapitre premier: Rapports entre conventions ou accords et lois et règlements.
========================================================================================

.. seealso::

   - :ref:`code_du_travail_P2`
   - :ref:`code_du_travail_P2_L2`
   - :ref:`code_du_travail_P2_L2_T5`

.. toctree::
   :maxdepth: 4
   

   L2251_1
