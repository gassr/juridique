
.. index::
   pair: Code du travail; Section syndicale

.. _code_du_travail_P2_L1_T4_CH2:
.. _section_syndicale:

=======================================================================================
Chapitre 2 section syndicale
=======================================================================================

.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/168221-section-syndicale
   - :ref:`code_du_travail_P2`
   - :ref:`code_du_travail_P2_L1`
   - :ref:`code_du_travail_P2_L1_T4`

.. toctree::
   :maxdepth: 4
   
   section2/index
