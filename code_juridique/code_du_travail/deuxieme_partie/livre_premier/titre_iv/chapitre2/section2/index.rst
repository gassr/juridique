
.. index::
   pair: Code du travail; Représentant de la section syndicale 
   pair: Code du travail; RSS 
   
.. _code_du_travail_P2_L1_T4_CH2_S2:
.. _representa,t_de_section_syndicale:

=================================================================================================================
Section 2 représentant de la section syndicale (RSS)
=================================================================================================================

.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/61-code-du-travail/496985/representant-de-la-section-syndicale
   - :ref:`code_du_travail_P2`
   - :ref:`code_du_travail_P2_L1`
   - :ref:`code_du_travail_P2_L1_T4`
   - :ref:`code_du_travail_P2_L1_T4_CH2`

.. toctree::
   :maxdepth: 4
   
   L2142_1_1
