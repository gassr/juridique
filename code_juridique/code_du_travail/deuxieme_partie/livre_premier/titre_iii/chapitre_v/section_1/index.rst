
.. index::
   pair: Les syndicats professionnels; Certification et publicité des comptes


.. _code_du_travail_P2_L1_T3_CH5_S1:

=========================================================================================================
Section 1 : Certification et publicité des comptes des organisations syndicales et professionnelles
=========================================================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/61-code-du-travail/511121/certification-et-publicite-des-comptes-des-organisations-syndicales-et-professionnelles
   - :ref:`code_du_travail_P2_L1`
   - :ref:`code_du_travail_P2_L1_T3`
   - :ref:`code_du_travail_P2_L1_T3_CH5`

.. toctree::
   :maxdepth: 4
   
   L2135_1
