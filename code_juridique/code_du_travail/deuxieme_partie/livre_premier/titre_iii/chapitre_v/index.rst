
.. index::
   pair: Les syndicats professionnels; statuts juridiques, resources et moyens


.. _code_du_travail_P2_L1_T3_CH5:

===================================================================================
Chapitre V : Ressources et moyens 
===================================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/495296-ressources-et-moyens
   - :ref:`code_du_travail_P2`   
   - :ref:`code_du_travail_P2_L1`
   - :ref:`code_du_travail_P2_L1_T3`

.. toctree::
   :maxdepth: 4
   
   section_1/index
