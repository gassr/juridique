
.. index::
   pair: Les syndicats professionnels; statuts juridiques, resources et moyens


.. _code_du_travail_P2_L1_T3:

===================================================================================
Titre III : statut juridique, ressources et moyens 
===================================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/494269-statut-juridique-ressources-et-moyens
   - :ref:`code_du_travail_P2`   
   - :ref:`code_du_travail_P2_L1`
   - :ref:`code_du_travail_P2_L1_T3`

.. toctree::
   :maxdepth: 4
   
   chapitre_v/index
