
.. index::
   pair: Code du travail; L.2111-1 
   pair: Code du travail; L.2111-2 


.. index::
   pair: Code du travail; Les syndicats professionnels 


.. _code_du_travail_P2_L1_T1_CU:

===================================================================================
Chapitre unique 
===================================================================================

.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/61-code-du-travail/168209/chapitre-unique
   - :ref:`code_du_travail_P2`
   - :ref:`code_du_travail_P2_L1`   
   - :ref:`code_du_travail_P2_L1_T1`    


.. _L.2111-1:

Article L2111-1 
===============

Les dispositions du présent livre sont applicables aux employeurs de droit 
privé ainsi qu'à leurs salariés.

Elles sont également applicables au personnel des personnes publiques employé 
dans les conditions du droit privé, sous réserve des dispositions particulières 
ayant le même objet résultant du statut qui régit ce personnel.


.. L.2111-2:

Article L2111-2 
================

Les dispositions du présent livre s'appliquent sans préjudice d'autres droits 
accordés aux syndicats par des lois particulières.
   
    
    
