
.. index::
   pair: Code du travail; Représentativité syndicale 


.. _code_du_travail_P2_L1_T2:

===================================================================================
Titre 2 représentativité syndicale
===================================================================================

.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/168212-syndicats-representatifs
   - :ref:`code_du_travail_P2`
   - :ref:`code_du_travail_P2_L1` 

.. toctree::
   :maxdepth: 4
   
   chapitre1/index
