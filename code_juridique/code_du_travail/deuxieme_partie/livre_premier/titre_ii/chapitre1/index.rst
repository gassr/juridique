
.. index::
   pair: Code du travail; Critères de représentativité

.. _code_du_travail_P2_L1_T2_CH1:
.. _criteres_de_representativite:

===================================================================================
Chapitre1 critères de représentativité
===================================================================================

.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/61-code-du-travail/168211/criteres-de-representativite
   - :ref:`code_du_travail_P2`
   - :ref:`code_du_travail_P2_L1` 
   - :ref:`code_du_travail_P2_L1_T2` 
   
.. toctree::
   :maxdepth: 4
   
   L2121_1
