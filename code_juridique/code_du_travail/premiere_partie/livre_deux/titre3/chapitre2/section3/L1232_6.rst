
.. index::
   pair: Code du travail;  L.1231-6
   pair: Notification du licenciement;  L.1231-6

.. _L.1232-6:

======================================================================
Article L1232-6: 
======================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/61-code-du-travail/176722/article-l1232-6
   - :ref:`code_du_travail_P1` 
   - :ref:`code_du_travail_P1_L2` 
   - :ref:`code_du_travail_P1_L2_T3` 
   - :ref:`code_du_travail_P1_L2_T3_CH2` 

   
Lorsque l'employeur décide de licencier un salarié, il lui notifie sa décision 
par lettre recommandée avec avis de réception.

Cette lettre comporte l'énoncé du ou des motifs invoqués par l'employeur.

Elle ne peut être expédiée moins de deux jours ouvrables après la date prévue 
de l'entretien préalable au licenciement auquel le salarié a été convoqué.

Un décret en Conseil d'Etat détermine les modalités d'application du présent 
article.
