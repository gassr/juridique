

.. index::
   pair: Code du travail; Notification du licenciement

.. _code_du_travail_P1_L2_T3_CH2_S3:

======================================================================
Section 3: notification du licenciement
======================================================================

.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/168141-licenciement-pour-motif-personnel
   - :ref:`code_du_travail_P1` 
   - :ref:`code_du_travail_P1_L2` 
   - :ref:`code_du_travail_P1_L2_T3` 
   
   
.. toctree::
   :maxdepth: 4
   
   L1232_6
