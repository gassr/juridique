

.. index::
   pair: Code du travail; Licenciement pour motif personnel

.. _code_du_travail_P1_L2_T3_CH2:

======================================================================
Chapitre2: licenciement pour motif personnel
======================================================================


.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/168141-licenciement-pour-motif-personnel
   - :ref:`code_du_travail_P1` 
   - :ref:`code_du_travail_P1_L2` 
   - :ref:`code_du_travail_P1_L2_T3`         

.. toctree::
   :maxdepth: 4
   
   section3/index
