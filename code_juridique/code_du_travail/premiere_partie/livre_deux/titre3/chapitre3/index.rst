
.. index::
   pair: Code du travail; Licenciement pour motif économique

.. _code_du_travail_P1_L2_T3_CH3:

======================================================================
Chapitre 3:  licenciement pour motif économique
======================================================================

.. seealso::

   - http://droit-finances.commentcamarche.net/legifrance/168142-licenciement-pour-motif-economique
   - :ref:`code_du_travail_P1` 
   - :ref:`code_du_travail_P1_L2`      
   - :ref:`code_du_travail_P1_L2_T3`   
   
.. toctree::
   :maxdepth: 4
   
   section1/index

