
.. index::
   pair: Code du travail; Rupture du contrat de travail à durée indéterminée


.. _code_du_travail_P1_L2_CH1:

======================================================================
Chapitre1  Dispositions générales
======================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/61-code-du-travail/168140/dispositions-generales
   - :ref:`code_du_travail_P1_L2`   

.. toctree::
   :maxdepth: 4
   
   L1231_1
