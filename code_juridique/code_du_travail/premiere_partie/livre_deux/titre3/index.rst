
.. index::
   pair: Code du travail; Rupture du contrat de travail à durée indéterminée

.. _code_du_travail_P1_L2_T3:

======================================================================
Titre3:  rupture du contrat de travail à durée indéterminée
======================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/168139-rupture-du-contrat-de-travail-a-duree-indeterminee
   - :ref:`code_du_travail_P1` 
   - :ref:`code_du_travail_P1_L2`     

.. toctree::
   :maxdepth: 4
   
   chapitre1/index
   chapitre2/index
   chapitre3/index
