
.. index::
   pair: Code du travail; Durée du travail, repos et congés

.. _code_du_travail_P3_L1_T2:

===============================================================================================
Titre II : durée du travail, répartition et aménagement des horaires
===============================================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/168331-duree-du-travail-repartition-et-amenagement-des-horaires
   - :ref:`code_du_travail_P3` 
   - :ref:`code_du_travail_P3_L1`   

.. toctree::
   :maxdepth: 4
   
   chapitre_premier/index

