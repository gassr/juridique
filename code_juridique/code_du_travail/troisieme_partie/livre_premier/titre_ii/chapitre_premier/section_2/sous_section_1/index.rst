


.. _code_du_travail_P3_L1_T2_CH1_S2_SS1:

===============================================================================================
Sous_section 2 : Durée légale et heures supplémentaires
===============================================================================================


.. seealso:: 

   - http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006902438&idSectionTA=LEGISCTA000006178001&cidTexte=LEGITEXT000006072050&dateTexte=20120124
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`   
   - :ref:`code_du_travail_P3_L1_T2`   
   - :ref:`code_du_travail_P3_L1_T2_CH1`

.. toctree::
   :maxdepth: 4
   
   L3121_10

