
.. index::
   pair: Code du travail; Durées maximales de travail

.. _code_du_travail_P3_L1_T2_CH1_S3:

===============================================================================================
Section 3 : Durées maximales de travail
===============================================================================================

.. seealso:: 

   - http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006902438&idSectionTA=LEGISCTA000006178001&cidTexte=LEGITEXT000006072050&dateTexte=20120124
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`   
   - :ref:`code_du_travail_P3_L1_T2`
   - :ref:`code_du_travail_P3_L1_T2_CH1`

.. toctree::
   :maxdepth: 4
   
   sous_section_1/index
   sous_section_2/index
   sous_section_3/index
