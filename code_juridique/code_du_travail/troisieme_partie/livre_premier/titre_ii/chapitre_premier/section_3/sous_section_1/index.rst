
.. index::
   pair: Code du travail; Temps de pause

.. _code_du_travail_P3_L1_T2_CH1_S3_SS1:

===============================================================================================
Sous-section 1 : Temps de pause
===============================================================================================

.. seealso:: 

   - http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006902438&idSectionTA=LEGISCTA000006178001&cidTexte=LEGITEXT000006072050&dateTexte=20120124 
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`     
   - :ref:`code_du_travail_P3_L1_T2_CH1`
   - :ref:`code_du_travail_P3_L1_T2_CH1_S3`
   
.. toctree::
   :maxdepth: 4
   
   L3121_33

