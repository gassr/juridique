
.. index::
   pair: Code du travail; Durées hebdomadaires maximales.

.. _code_du_travail_P3_L1_T2_CH1_S3_SS3:

===============================================================================================
Sous-section 3 : Durées hebdomadaires maximales.
===============================================================================================

.. seealso:: 

   - http://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006902473&dateTexte=&categorieLien=cid
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`     
   - :ref:`code_du_travail_P3_L1_T2_CH1`
   - :ref:`code_du_travail_P3_L1_T2_CH1_S3`
   
.. toctree::
   :maxdepth: 4
   
   L3121_35
   L3121_36
