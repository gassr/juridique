
.. index::
   pair: Code du travail; Durée quotidienne maximale

.. _code_du_travail_P3_L1_T2_CH1_S3_SS2:

===============================================================================================
Sous-section 2 : Durée quotidienne maximale
===============================================================================================

.. seealso:: 

   - http://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006902473&dateTexte=&categorieLien=cid
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`     
   - :ref:`code_du_travail_P3_L1_T2_CH1`
   - :ref:`code_du_travail_P3_L1_T2_CH1_S3`
   
.. toctree::
   :maxdepth: 4
   
   L3121_34

