
.. index::
   pair: Code du travail; Durée du travail

.. _code_du_travail_P3_L1_T2_CH1:

===============================================================================================
Chapitre Ier : Durée du travail
===============================================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/168332-duree-du-travail
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`   
   - :ref:`code_du_travail_P3_L1_T2`


.. toctree::
   :maxdepth: 4
   
   section_1/index
   section_2/index
   section_3/index
   section_4/index
