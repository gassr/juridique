
.. index::
   pair: Code du travail; Conventions de forfait sur l'année

.. _code_du_travail_P3_L1_T2_CH1_S4_SS2:

===============================================================================================
Sous-section 2 : Conventions de forfait sur l'année
===============================================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/171951-conventions-de-forfait
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`   
   - :ref:`code_du_travail_P3_L1_T2`
   - :ref:`code_du_travail_P3_L1_T2_CH1`

.. toctree::
   :maxdepth: 4
   
   paragraphe_1/index
   paragraphe_2/index   

