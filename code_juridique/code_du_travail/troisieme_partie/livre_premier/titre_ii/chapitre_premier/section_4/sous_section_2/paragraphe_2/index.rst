
.. index::
   pair: Code du travail; Conventions de forfait en jours sur l'année

.. _code_du_travail_P3_L1_T2_CH1_S4_SS2_PARA2:

===============================================================================================
Paragraphe 2 : Conventions de forfait en jours sur l'année
===============================================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/61-code-du-travail/498870/conventions-de-forfait-en-jours-sur-l-annee
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`   
   - :ref:`code_du_travail_P3_L1_T2`
   - :ref:`code_du_travail_P3_L1_T2_CH1`
   - :ref:`code_du_travail_P3_L1_T2_CH1_S4`
   - :ref:`code_du_travail_P3_L1_T2_CH1_S4_SS2`
      
.. toctree::
   :maxdepth: 4
   
   L3121_43
   


