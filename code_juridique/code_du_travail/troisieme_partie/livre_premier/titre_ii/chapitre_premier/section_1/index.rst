
.. index::
   pair: Code du travail; Travail effectif, astreintes et équivalences

.. _code_du_travail_P3_L1_T2_CH1_S1:

===============================================================================================
Section 1 : Travail effectif, astreintes et équivalences
===============================================================================================

code_du_travail_P3_L1_T2_CH1_S1

.. seealso:: 

   - http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006902438&idSectionTA=LEGISCTA000006178001&cidTexte=LEGITEXT000006072050&dateTexte=20120124
   - :ref:`code_du_travail_P3`  
   - :ref:`code_du_travail_P3_L1`   
   - :ref:`code_du_travail_P3_L1_T2`   
   - :ref:`code_du_travail_P3_L1_T2_CH1`

.. toctree::
   :maxdepth: 4
   
   L3121_1

