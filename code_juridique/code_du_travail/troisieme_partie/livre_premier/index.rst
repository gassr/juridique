
.. index::
   pair: Code du travail; Durée du travail, repos et congés

.. _code_du_travail_P3_L1:

===============================================================================================
Livre premier : durée du travail, repos et congés
===============================================================================================

.. seealso:: 

   - http://droit-finances.commentcamarche.net/legifrance/167528-duree-du-travail-salaire-interessement-participation-et-epargne-salariale
   - :ref:`code_du_travail_P3` 

.. toctree::
   :maxdepth: 4
   
   titre_ii/index

