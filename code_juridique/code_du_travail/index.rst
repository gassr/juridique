
.. index::
   Juridique (code du travail)

.. _code_du_travail:

===============
Code du travail
===============

.. seealso:: 

   - http://www.legifrance.gouv.fr/affichCode.do;jsessionid=DDBBB3A6F929AFC69C706A4C860DB83A.tpdjo07v_1?cidTexte=LEGITEXT000006072050&dateTexte=20111126
   - http://droit-finances.commentcamarche.net/legifrance/281-partie-legislative-nouvelle
   - https://fr.wikipedia.org/wiki/Code_du_travail
   - https://fr.wikipedia.org/wiki/Code_du_travail_%28France%29


Le Code du travail est un code juridique qui définit un cadre aux relations de travail.
En France, les codes juridiques sont consultables sur le site Légifrance_.

Le code du travail français est un recueil organisé de la plupart des textes 
législatifs et réglementaires applicables en matière de droit du travail, et 
qui concerne essentiellement les salariés sous contrat de travail de droit 
privé, les salariés du secteur public étant généralement soumis à des statuts 
particuliers.

Il existe aussi en dehors du code du travail des textes touchant de près ou 
de loin au droit du travail (dans la plupart des cas, des décrets ou arrêtés). 

Mais il est indispensable de prendre désormais en compte les textes européens, 
qu'il s'agisse des directives ou des traités notamment. 

De même les accords et conventions collectives jouent un rôle normateur de 
plus en plus important avec l'évolution du dialogue social.
.

.. _codex: https://fr.wikipedia.org/wiki/Codex
.. _Légifrance:  https://fr.wikipedia.org/wiki/L%C3%A9gifrance



.. _sommaire_code_du_travail_legifrance:

===================================
Sommaire Code du travail légifrance
===================================

.. seealso:: 

   - http://www.legifrance.gouv.fr/affichCode.do;jsessionid=DDBBB3A6F929AFC69C706A4C860DB83A.tpdjo07v_1?cidTexte=LEGITEXT000006072050&dateTexte=20111126
   - http://droit-finances.commentcamarche.net/legifrance/281-partie-legislative-nouvelle


.. toctree::
   :maxdepth: 8
   
   premiere_partie/index
   deuxieme_partie/index
   troisieme_partie/index   
   
