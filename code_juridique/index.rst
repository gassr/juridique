
.. index::
   Juridique (code juridique)

.. _code_juridique:

===============
Code juridique 
===============

.. seealso:: 

   - https://fr.wikipedia.org/wiki/Code_juridique


En droit, un code est un recueil de lois ou de règles (code d'honneur) définies 
par un groupe, une société, un métier, un État.

L'origine du mot code provient de sa polysémie : c'est une extension du sens du 
mot codex_ .

En France, les codes juridiques sont consultables sur le site Légifrance_ .


.. _codex: https://fr.wikipedia.org/wiki/Codex
.. _Légifrance:  https://fr.wikipedia.org/wiki/L%C3%A9gifrance


.. toctree::
   :maxdepth: 4
   
   code_du_travail/index
