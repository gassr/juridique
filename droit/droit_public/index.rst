
.. index::
   Droit public

.. _droit_public:

============
Droit public 
============

.. seealso::
 
   - http://fr.wikipedia.org/wiki/Droit_public
  

Le droit public est constitué par l'ensemble des règles régissant les rapports 
de droit dans lesquels interviennent des personnes morales de droit public 
comme l'État, les collectivités locales, des institutions ou groupement 
spécifiquement rattachée au droit public ou des banques nationales. 

Le droit public défend l'intérêt général avec des prérogatives liées à 
la puissance publique. 

Il concerne les rapports entre deux personnes publiques mais également entre 
une personne publique et une personne privée.

Le droit public est en général opposé au droit privé, qui lui recouvre 
l'ensemble des règles qui régissent les rapports entre les personnes 
physiques ou morales.




