
.. index::
   pair: Droit privé ; Droit du travail

.. _droit_du_travail:

=================
Droit du travail 
=================

.. contents::
   :depth: 3


Introduction
============

.. seealso::
 
   - http://fr.wikipedia.org/wiki/Droit_du_travail
   - https://fr.wikipedia.org/wiki/Portail:Droit_fran%C3%A7ais/Droit_du_travail
 

Le droit du travail est l'ensemble des normes juridiques qui régissent les 
relations entre un employeur et un travailleur. Le droit du travail encadre 
notamment la formation, l'exécution et la rupture du contrat de travail. 

Il garantit également le respect des **libertés syndicales** et des normes de 
sécurité au travail, et la protection des travailleurs vulnérables.

Droit du travail en France
==========================

.. seealso::
 
   - http://fr.wikipedia.org/wiki/Droit_du_travail_en_France
 
Le droit du travail est une branche du droit social qui régit les relations 
nées d'un contrat de travail entre les employeurs et les salariés. 

En France, ces relations sont caractérisées par l'existence d'un lien de 
subordination juridique des salariés à leurs employeurs. 

Dès lors, le droit du travail a pour objet d'encadrer cette subordination et 
de limiter le déséquilibre entre les parties au contrat de travail. 

Les règles du droit du travail ne s'appliquent pas aux agents titulaires et 
contractuels de droit public, et aux travailleurs indépendants.

Le droit du travail comporte certains enjeux politiques, économiques et 
sociaux. 

En déterminant les conditions d'emploi actuelles des salariés, le droit du 
travail exerce une influence sur le marché de l'emploi présent et à venir. 
Il exerce également une influence sur la compétitivité économique des 
entreprises et de l'économie nationale. 


Une complexité conséquente des règles et des domaines
=====================================================

L'histoire du droit français du travail est une l'une des origines de sa 
complexité. Son histoire mouvementée est l'origine de la diversité de ses 
sources et de son manque d'unité. 

En effet, le droit français du travail ne possède pas réellement de théorie 
générale unie et acceptée par la majorité. Le droit du travail français est 
plus pragmatique s'est composé en différentes thématiques regroupées en 
deux blocs.

Le premier bloc : une relation individuelle
-------------------------------------------

Le bloc constitue ce que l'on nomme les relations individuelles, car elles ne 
concernent généralement que deux individus (le salarié, et l'employeur).

Il s'agit de tout ce qui concerne le recrutement, l'embauche et les différentes 
phases du contrat de travail (conditions de fond et de forme, conditions de 
validité, exécution du contrat (principalement les conditions de travail, fin 
du contrat par le licenciement en général mais il existe d'autres modes de rupture). 

Sans oublier l'après-contrat (clause de non-concurrence, chômage...) 

C'est d'ailleurs ce qui fait dire que le droit du travail est étroitement lié 
au droit de la sécurité sociale. Mais aussi tout ce qui a trait à l'emploi ou 
au placement des salariés (types de contrat, statuts des salariés...) sans 
parler de la formation professionnelle.

Le deuxième bloc : une relation collective
------------------------------------------

Le deuxième bloc couvert par le droit du travail concerne quant à lui ce que 
l'on nomme les relations collectives. 

Notamment, tout ce qui a trait aux conflits collectifs (les grèves, les 
restructurations etc...leur déroulement et leurs conséquences sont régies 
par le droit du travail) ainsi que tout ce qui a trait à la négociation 
collective (représentants du personnel, règles de la négociation, 
accords professionnels, conventions collectives...).


Accords collectifs
==================

.. toctree::
   :maxdepth: 3
   
   accords_collectifs/index
   
   

