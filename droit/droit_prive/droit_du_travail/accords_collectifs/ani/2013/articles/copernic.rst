
.. index::
   pair: Copernic; ANI 2013

.. _copernic_ani_2013:
 
========================================================================
Copernic, Un accord "donnant-perdant" pour les salariés : l’ANI décrypté
========================================================================


.. seealso::

   - http://www.fondation-copernic.org/spip.php?article838
   - http://www.france.attac.org/articles/un-accord-donnant-perdant-pour-les-salaries-lani-decrypte
   - http://www.fondation-copernic.org/IMG/pdf/Accord-ANI-COPERNIC.pdf
   
  
.. contents::
   :depth: 3


Introduction
============

Rarement un accord national interprofessionnel (ANI) aura entériné autant 
de reculs pour les salarié-e-s que celui conclu le 11 janvier 2013 entre 
le patronat et trois syndicats. 

La Fondation Copernic met à la disposition de tou-te-s un décryptage 
complet du texte de l’accord (ci-dessous_) et lance avec Attac un appel 
à la mobilisation.    
   
.. _ci-dessous: http://www.fondation-copernic.org/IMG/pdf/Accord-ANI-COPERNIC.pdf    

Synthèse
========

Les « nouveaux droits » qui figurent dans l’accord sont, en effet, de 
portée très limitée, tandis que des revendications patronales de grande 
portée sont satisfaites. 

Quels sont donc ces nouveaux « droits » ? 

La fameuse majoration de cotisation (incorrectement nommée « taxation ») 
des contrats à durée déterminée (CDD) courts est limitée : + 0,5 à + 3 
points en fonction des types ou des durées de contrat, de moins d’un 
mois à trois mois. 
Elle peut aisément être contournée : en allongeant la durée des contrats 
les plus courts au-delà des seuils de majoration ; en recourant au CDD 
plus fréquemment pour le remplacement d’un salarié absent (pas de majoration) 
que pour un accroissement temporaire d’activité (majoration) ou en 
remplaçant les CDD par des contrats d’intérim, qui peuvent être conclus 
pour les mêmes motifs que les CDD et qui ne feront pas l’objet de 
majoration. Difficile de croire, par conséquent, à l’efficacité de 
cette mesure. Le coût de ce dispositif est estimé à 110 millions d’euros 
pour le patronat, mais il obtient en compensation une réduction de 
cotisations sociales de 155 millions d’euros pour les embauches en CDI 
de jeunes de moins de 26 ans.

On pourrait faire la même démonstration sur la quasi-totalité des 
conquêtes de papier de l’accord. Les droits rechargeables pour les 
chômeurs ? La discussion concrète se fera avec la renégociation de la 
convention UNEDIC, sans « aggraver le déséquilibre financier du régime 
d’assurance chômage ». 

Traduction : ce qui sera donné à certains chômeurs sera enlevé à 
d’autres. 

La couverture santé complémentaire généralisée ? Elle est renvoyée à la
négociation de branche, et en cas d’échec, ce n’est qu’en 2016 que 
toutes les entreprises seraient tenues d’assurer la couverture d’un 
« panier de soins » limité, le tout financé à moitié par les salarié-e-s.


Le patronat, quant à lui, peut se féliciter de l’accord. Il gagne à la 
fois une plus grande flexibilité et une plus grande sécurité juridique. 
Tout d’abord la conclusion d’« accords de compétitivité-emploi » souhaités 
par Sarkozy, qualifiés d’« accords de maintien dans l’emploi », est 
rendue possible. Un accord pourra, ainsi, prévoir une baisse de salaire 
en échange du maintien de l’emploi. 
**Le salarié qui refusera sera licencié pour motif économique.**
La « cause réelle et sérieuse » sera en fait l’accord lui-même et 
l’employeur sera exonéré de l’ensemble des obligations légales et 
conventionnelles attachées au licenciement économique.

De fait, le contrat de travail ne pourra plus résister à l’accord 
collectif, même si ce dernier est moins favorable au salarié. 
L’inversion de la hiérarchie des normes et la destruction du principe 
de faveur se poursuivent. Il devient également possible de déroger par 
accord d’entreprise aux procédures de licenciement économique collectif. 
L’employeur aura d’ailleurs le choix de rechercher l’accord avec les 
syndicats ou de soumettre directement ses souhaits en matière de 
procédure et de contenu du plan social à l’homologation de l’administration.

Les délais de contestation sont drastiquement raccourcis : 3 mois pour 
contester l’accord ou l’homologation (12 mois actuellement), 12 mois 
pour un-e salarié-e contestant son licenciement (5 ans actuellement). 
Tout est fait pour éviter que le juge judiciaire s’en mêle, pour 
« sécuriser les relations de travail », comme le dit le MEDEF. 

Cerise sur le gâteau, l’article 26 limite l’accès au juge prud’homal : 
instauration d’un délai de 2 ans maximum pour une réclamation portant 
sur l’exécution ou la rupture du contrat de travail (sauf discrimination) 
et de 3 ans pour une demande de salaire en cours d’exécution du contrat 
(contre 5 ans actuellement).

Enfin, les prérogatives des comités d’entreprises sont réduites par 
diverses dispositions et un délai de trois mois supplémentaire est 
accordé aux employeurs pour organiser l’élection des délégués du 
personnel et des membres du comité d’entreprise une fois atteint 
l’effectif déclenchant l’obligation, ainsi qu’un délai d’un an pour 
respecter la totalité des obligations liées au dépassement des seuils 
d’effectif de 11 et de 50 salarié-e-s : un comble !

Cet accord ne fera pas reculer la précarité, ni le chômage, ne créera 
pas d’emploi, mais fera régresser un peu plus les droits des salarié-e-s, 
à commencer par les plus précaires d’entre eux, les femmes en particulier. 

Il a été signé par trois organisations syndicales n’ayant rassemblé aux 
dernières élections prud’homales que 38,7% des voix (et 28,11 % au 
récent référendum de représentativité organisé dans les TPE-PME). 

Si les règles de validité permettent actuellement de considérer cet 
accord comme « majoritaire », il apparaît éminemment problématique 
qu’il puisse être repris dans la loi, alors même qu’il n’est signé 
que par des organisations ne représentant qu’une minorité de syndicats 
et de salarié-e-s. 

La majorité de gauche au Parlement n’a pas été élue pour mener une 
politique d’inspiration aussi nettement libérale.

La Fondation Copernic et Attac ont décidé, par conséquent, d’appeler 
tous les acteurs du mouvement social et syndical, ainsi que des partis 
de la gauche et de l’écologie politique à organiser la mobilisation 
nécessaire pour s’opposer à cet accord.
