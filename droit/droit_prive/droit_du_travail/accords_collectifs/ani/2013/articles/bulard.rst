
.. index::
   pair: Bulard; ANI 2013

.. _bulard_ani_2013:
 
===========================================================================================================================
Rien contre le chômage, Droit social à la moulinette, Martine Bulard, jeudi 17 janvier 2013
===========================================================================================================================


.. seealso::

   - http://www.monde-diplomatique.fr/carnet/2013-01-17-Droit-social
  
  
.. contents::
   :depth: 3


Introduction
============

**Historiquement régressif.**

On a beau chercher les mots les plus nuancés, 
on ne peut en trouver d’autres pour qualifier l’accord concocté par, 
d’une part, le Mouvement des entreprises de France (Medef), la 
Confédération générale du patronat des petites et moyennes entreprises 
(CGPME), et, d’autre part, trois des cinq syndicats salariés invités 
autour de la table : la Confédération française démocratique du travail 
(CFDT), la Confédération générale des cadres (CGC) et la Confédération 
française des travailleurs chrétiens (CFTC). 

Deux syndicats — la Confédération générale du travail (CGT) et 
Force ouvrière (FO) — ont rejeté l’accord. 

La Fédération syndicale unitaire (FSU), Solidaires (Sud) et l’Union 
nationale des syndicats autonomes (UNSA) étaient d’emblée hors course, 
car considérés comme « non représentatifs » selon la nouvelle loi
