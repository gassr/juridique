
.. index::
   pair: CGA; ANI

.. _cga_ani_2013:
 
===========================================================================================================================
L'ANI, un net progrès... pour le droit d'exploiter ! vendredi 3 mai 2013
===========================================================================================================================

.. contents::
   :depth: 3


Courriel
========

::

    Sujet:  (fr) Coordination des Groupes Anarchistes CGA, Monde du travail - L'ANI, un net progrès... pour le droit d'exploiter ! (en)
    Date :  Fri, 03 May 2013 10:56:29 +0300
    De :    a-infos-fr@ainfos.ca
    Répondre à :    a-infos-fr@ainfos.ca
    Pour :  fr <a-infos-fr@ainfos.ca>

Introduction
============

La lutte des classes est plus que jamais d'actualité : l'Etat, le Patronat 
et leurs alliés  (syndicats jaunes, médias bourgeois...) utilisent toutes 
les armes à leur disposition pour  attaquer les intérêts de la classe 
populaire, quelle que soit la couleur du gouvernement.
 
L’Accord National Interprofessionnel (ANI) constitue un coup très grave 
dans cette guerre sociale. À l'heure où le patronat tire le meilleur 
parti de la crise en licenciant à  tour de bras, le projet de loi nommé 
Accord National Interprofessionnel (ANI) est conçu  pour rendre plus 
facile les procédures de licenciements et affaiblir les possibilités de 
recours dont disposent les travailleur·euse·s. 

Ce n'est pas qu'une petite étape de plus dans la casse du droit du 
travail, car ce projet de loi est très dangereux, plus régressif par 
exemple que les accords de déréglementation du marché du travail de 2008.

Le patronat en rêve, l’État le fait
====================================

Sarkozy, à la demande du patronat, lance en janvier 2012 la négociation
pour des «accords compétitivité-emploi». Devant les salarié·e·s de 
Gandrange, Hollande-candidat promet que «ces accords ne verront pas le jour». 

Pourtant, en octobre 2012 Ayrault et Hollande envoient aux syndicats un 
document de travail sur la sécurisation de l'emploi leur  enjoignant de 
reprendre les négociations qui n'avaient pu aboutir au printemps 2012 
faute de temps. Les négociations ont lieu au siège du Medef, syndicat 
patronal le plus puissant, sur la base d'un texte rédigé par ce dernier.

Le 10 Janvier 2013 la CFDT ainsi que deux autres syndicats habitués des 
trahisons au sommet signent une version légèrement remaniée des textes 
présentés par le Medef. La porte-parole du patronat applaudit des deux 
mains et exige que cet accord *soit respecté à la lettre*. Hollande se 
déclare satisfait et demande *au gouvernement de transcrire fidèlement 
les dispositions d'ordre législatif prévues* dans l'accord. 
Le  délégué de la CFDT déclare sans rire qu'il s'agit «d'un accord 
ambitieux pour faire reculer la précarité» ! 

**Et pourtant, c'est exactement l'inverse**.

L'ANI semble complexe mais en réalité c'est très simple : dans un contexte 
où les patrons  se servent de la crise comme prétexte pour licencier, il 
s'agit pour eux de pouvoir licencier plus librement et au moindre coût. 

**Bref, de mettre une trappe sous chacun des postes de travail, sans avoir 
à trop débourser pour les vies ainsi brisées**. 
Il s'agit donc bien d'aggraver la précarité, au cœur du système. 
L'ANI est une machine à fabriquer toujours plus de chômage. 
Précisons que nous sommes tout·e·s concerné·e·s.

Chantage à l'emploi puissance mille
===================================

Article 15
----------

Les patrons pourront licencier sans avoir à justifier d'un motif 
économique tout·e salarié·e qui refuse une modification de son poste ou 
de son lieu de travail (Art.15). 

Le licenciement se fera pour motif personnel. Le ou la salarié·e sera 
jugé·e responsable de son licenciement pour refus de mobilité. 
Enfin la clause de mobilité n'est pas limitée en terme de distance ou 
de temps de déplacement: les patrons auront donc les mains libres 
pour licencier à leur bon vouloir.

Article 18
----------

En échange de la seule promesse de ne pas licencier pour deux ans, les 
patrons pourront nous faire travailler plus longtemps et/ou nous payer 
moins (Art. 18). 

Pire que le projet Sarkozy sur le sujet qui donnait la possibilité aux 
patrons de nous faire travailler plus pendant un an, mais sans toucher 
aux salaires (loi Warsmann, JO du 22 mars, art. 40). 
Précisons qu'il ne s'agit de la part des patrons que d'une promesse, et 
limitée à 2 ans. 
Rien ne les empêche de licencier malgré tout, comme le montre l'exemple
de Continental à Amiens. Le patronat espère ainsi diminuer la part des 
salaires dans les richesses crées par l'entreprise, ce qui sera autant 
de bénéfice en plus pour les patrons et les actionnaires. C'est aussi 
un moyen pernicieux d'en finir avec la durée légale du travail.

Un exemple de ce que pourrait donner ces accords dits de «maintien de 
l'emploi» nous est donné par un accord signé chez Renault début mars. 
Je cite ici la Tribune syndicaliste libertaire, animée par des membres 
de la CGA: «salué par la presse bourgeoise comme un "premier accord du 
genre au niveau d'un groupe, qui pourrait devenir une référence en la 
matière" (Le Parisien), celui ci prévoit une augmentation du temps de 
travail des salarié·e·s de 6,5%, avec évidemment le gel des salaires 
en 2013. En contrepartie (sic), le groupe Renault s'est engagé [pour 2013] 
à ne pas fermer de sites, à ne pas recourir à de plan social [...]. 
Par contre il maintient les 8 260 suppressions de postes prévues d'ici 
à la fin de 2016 (**soit 15 % des effectifs**).»


Art. 18 toujours
----------------

Les patrons pourront licencier les salarié·e·s qui refusent les 
conséquences de ces accords dits de «maintien de l'emploi». 
Le licenciement se fera pour motif économique, dont la justification 
suffisante sera le refus de l'accord précité (!). 

Là aussi il s'agit de décider à la place des prud'hommes le motif du 
licenciement. Mais le pire c'est que l'ANI prévoit que les patrons sont 
alors exonérés de l'ensemble des obligations légales et conventionnelles 
qui auraient résulté d'un licenciement pour motif économique.


Articles 20, 23
----------------

Et si aucun accord «de maintien de l'emploi» n'est signé avec les syndicats, 
l'ANI exonère tout de même les patrons des obligations légales et 
conventionnelles liés au licenciement économique en cas de licenciement 
collectif, pour peu que le document produit par l'employeur soit homologué 
par le DIRRECTE. 
L'homologation peut seulement être implicite et exclut tout contrôle sur 
le motif des licenciements. Les charges de famille, l'ancienneté, l'âge 
et le handicap ne sont plus des références obligatoires pour savoir 
qui sera licencié et qui sera gardé. L'ordre des licenciements sera 
dorénavant fixé par le patron (Art.23).

Les rêves du patronat sont nos cauchemars
=========================================

Article 25
----------

En cas de contestation d'un licenciement auprès des prud'hommes l'ANI 
prévoit une procédure de conciliation entre patrons et salarié·e·s, qui 
débouchera au mieux sur le versement d'une somme plafonnée, fixée par 
un barème strict en fonction de l'ancienneté, ce qui revient à limiter 
son montant. C'est seulement si aucun accord n'est trouvé à ce moment là 
que le ou la salarié·e pourra aller devant les juges des prud'hommes. 
Mais attention à ne pas perdre trop de temps en route car (Art. 26) le 
délai qu'ont les salarié·e·s pour recourir aux prud'hommes afin de 
contester un licenciement passe de 5 à 2 ans. Le délai pour que les 
salarié·e·s récupèrent les salaires qui leur sont dus passe de 5 à 3 ans 
(à compter du licenciement). Il est clair qu'avec ces nouvelles conditions 
beaucoup de salarié·e·s n'auraient pas pu obtenir gain de cause par le passé.

Possibilité pour les patrons d'entreprises de moins de 50 salarié·e·s de 
recourir unilatéralement à des CDI intermittents, dans trois secteurs. 
Deux jours avant de signer l'accord, le négociateur de la CFDT déclarait 
pourtant: « Nous sommes formellement opposés à la création de CDI de 
projet et de CDI intermittents. 
Nous savons bien que ces contrats deviendraient la norme, et signeraient 
donc une nouvelle précarisation des salarié·e·s. » 
**Il a finalement retourné sa veste, du jour au lendemain.**

Article 12
-----------

Les élu·e·s du personnel se voient imposer la «confidentialité» sur les 
informations qu'ils et elles reçoivent dans le cadre de la consultation 
anticipée... anticipée par rapport à quoi ? Par rapport aux charrettes 
de licenciement ?

Petites avancées ou bien reculs déguisés ?
==========================================

Un des obstacles à la mobilisation est la propagande médiatique qui 
complexifie cet accord et le présente comme étant plus ou moins «équilibré». 
Mais les quelques prétendues «petites avancées pour les salariés» qu'il 
contient sont en fait de vrais cadeaux au patronat.

Complémentaire santé pour tou·te·s
===================================

On entend parler d'une *complémentaire santé pour tou·te·s*. Il s'agit 
en fait d'une mince couverture pour des frais de santé sur un panier de 
soin très limité. Financée par les salarié·e·s et les patrons à 50/50. 
Seulement, c'est le patron qui décidera unilatéralement où vont les fonds. 
Il y a fort à parier que cette somme n'ira donc pas aux  mutuelles mais 
aux assureurs privés comme Axa, Allianz et autres, qui se réjouissent de 
ce nouveau marché. Il s'agit bien d'une mise en concurrence de la sécu 
et des mutuelles avec les grandes entreprises d'assurance. Il est vrai 
que la direction de la CFDT se prononce discrètement mais régulièrement 
pour la «mise en concurrence de lasécurité sociale».

Sur la surtaxation des CDD
==========================

Aucune victoire ! Le coût pour l'assurance chômage de nouvelles 
exonérations de cotisations patronales pour les  «CDI jeune» 
(150 millions) excèdera ce que rapportera l'augmentation 
des cotisations patronales sur les seuls CDD inférieurs à trois mois 
(100 millions, chiffres Medef). De plus, les contrats saisonniers 
ou les CDD de remplacement ou les CDD supérieurs à 3 mois ne sont pas 
concernés par ces très légères surcotisations (entre 4 et 7%). 
Mais surtout l’intérim n'est pas concerné, or le coût d'un contrat 
intérim sera désormais de 15% inférieur à un contrat CDD court 
classique. Au lieu de recourir à un CDD court les employeurs feront 
appel encore plus à l'intérim, offrant de nouvelles perspectives de 
profit pour Manpower et compagnie. 
Enfin les jeunes embauché·e·s en CDI court peuvent de toute façon être 
licencié·e·s au bout de trois mois, les périodes d'essai ayant été 
allongé par des lois antérieures.

Le Medef n'a pas concédé les droits rechargeables à l'assurance chômage, 
car l'accord engage seulement à une nouvelle discussion lors de la 
prochaine négociation Unédic. De plus, l'accord précise que si cela 
devait se mettre en place, les comptes de l'Unedic dévraient rester 
équilibrés, ce qui laisserait entrevoir une diminution d'autres dépenses 
et donc une baisse générale des indemnités chômage. 

Compte personnel de formation, DIF
==================================

Quant au «compte personnel de formation» qui selon la propagande médiatique 
suivrait le ou la salarié·e même si celui-ci ou celle-ci passe par des 
périodes de chômage, il se limite à 20h par an. Or il existe déjà un droit 
individuel à la formation de 20h par an, et les deux ne seront bien 
sûr pas cumulables. 

Enfin on peut craindre que les procédures de suivi des salarié·e·s 
«tout au long de la vie» à travers différents comptes, 
«passeports formation» et autres «livrets de compétences», ne se 
transforme petit à petit en un flicage pour le compte des patrons qui 
eux n'attendent que cela, le retour du livret ouvrier avec tout le 
pedigree des candidats à l'embauche.

Heures complémentaires
======================

L'accord échange un dédommagement financier des heures supplémentaires 
légèrement amélioré... contre la possibilité pour les patrons de recourir 
à des «heures complémentaires» à la place des heures sup', et dont le
taux de majoration sera négocié par accord de branche (Art.11), dans la 
limite de huit périodes d'heures complémentaires par an. Ou comment 
faire croire que l'on donne alors que l'on reprend...

Nous ne sommes pas des kleenex !
================================

L'ANI ne sera pas adopté définitivement avant mai 2013. Mais le temps 
presse. Rien à attendre des député·e·s, qui nous le savons bien voteront 
le texte après peut-être quelques petits amendements. 
Ce texte n'est pas amendable, il est du début à la fin une catastrophe 
pour les salarié·e·s et les masses de chômeur·euse·s ! 

Quant à la future loi PS «contre les licenciements» elle est n'est qu'une 
promesse de plus,destinée à faire passer la pilule de l'ANI auprès des 
directions syndicales et de l'aile gauche du PS, ainsi qu'à calmer la 
colère sociale. Il s'agira en fait d'une mesurette absolument 
inoffensive: elle portera sur les licenciements par les sites rentables, 
or un des effets de l'ANI est de permettre aux patrons de qualifier tous 
les licenciements d'économiques ou personnels...

Enfermées dans une stratégie de lobbying auprès des députés, il n'y a 
réellement aucun espoir que les directions de la CGT ou de FO aillent 
plus loin que des appels sporadiques à des journées d'actions sans grève, 
au mieux.

Comme le dit le groupe de Lyon_dans son tract : «L’expérience du dernier 
mouvement des retraites doit être mise à profit : ce dont nous avons 
besoin, c’est du développement de la grève active, avec blocage de la 
production, des transports. 
C'est la multiplication des initiatives populaires, dans les entreprises, 
les quartiers, les communes, fondées sur le rapport de force et non 
l’appel illusoire à "l’intervention des élu·e·s". 

Il nous faut donc dès maintenant, tout en participant à développer dans 
nos syndicats les conditions d’une lutte de masse, créer des liens 
intersyndicaux, interprofessionnels à la base permettant de faire face 
en toutes circonstances aux manœuvres bureaucratiques, et développant 
l’auto-organisation populaire. A côté de ce travail dans les syndicats et 
entre syndicalistes et syndiqué·e·s de base, il nous faut développer des 
espaces d’organisation permettant d’entraîner les non syndiqué·e·s dans
la lutte, et la coordination de l’action entre tous les secteurs des 
classes populaires.»

Les SANOFI de Toulouse et Montpellier ont remporté récemment une petite
victoire judiciaire, mais si l'ANI passe, ils sont cuits. 
J'espère bien avoir montré pourquoi L'ANI est une arme puissante contre 
toutes celles et ceux qui sont actuellement en lutte contre les 
licenciements dans leurs boîtes, et pour l'ensemble des 
travailleur·euse·s d'aujourd'hui ou de demain.

::

    Hugo
    (Groupe A. Camus, Toulouse)
