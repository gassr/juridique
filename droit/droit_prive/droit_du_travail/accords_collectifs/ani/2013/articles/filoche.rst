
.. index::
   pair: Copernic; ANI 2013

.. _filoche_ani_2013:
 
===========================================================================================================================
Rien contre le chômage, le Medef a bloqué toute avancée pour les salariés dans cet accord signé par une minorité syndicale
===========================================================================================================================


.. seealso::

   - http://www.filoche.net/2013/01/12/rien-contre-le-chomage-le-medef-a-bloque-tout-avancee-pour-les-salaries-dans-cet-accord-axa-signe-par-une-minorite-syndicale/
  
  
.. contents::
   :depth: 3


Introduction
============

analyse revue et corrigée, détaillée et complétée de l’ANI Accord 
National Interprofessionnel « accords de Wagram » (rédigée du samedi 12 
au mercredi 16 janvier) + 175 commentaires, ça débat !

Les « accords de Wagram » (l’ANI s’est discuté au siège du patronat) 
ne croyez pas les grandes phrases des médias qui les encensent. 
Ceux qui le défendent font du grand baratin mais ne le citent jamais. 
Comme avec les contrats d’assurance, il faut bien lire ce qu’il y a de 
marqué en tout petit. Ils mettent beaucoup de beurre sur la tartine 
mais c’est en la croquant qu’on découvre qu’elle est moisie.

Alors lisez, lisez. Ne vous contentez pas d’une présentation superficielle.
Dés qu’on prend le temps de les lire, on perçoit la nature réelle de cet 
accord, on est effaré.

L’accord est organisé en 28 articles, inégaux. Prenez le temps 
ci-dessous, on a regroupé en 13 points. Un accord de flexibilisation 
forcée. Un accord de chantage à l’emploi. Une attaque contre le CDI. 

Un accord de sécurisation de la délinquance patronale. Lisez. 
Le diable est dans les détails.

Ce sont des accords régressifs, signés par une minorité de syndicalistes 
et ils ne feront pas un seul chômeur en moins, pas un emploi en plus. 
Du point de vue de l’inversion de la courbe du chômage en 2013, ils sont 
dangereux. S’ils s’appliquent (car rien n’est fait et même ce qui est 
prévu est étalé dans le temps de fin 2013 à 2016… et l’essentiel des 
points sont prévus.. à renégociation ultérieure) ils donneront lieu à 
des charrettes empressées de licenciements – comme Mittal, Petroplus, 
Sanofi. 

Le Medef veut battre politiquement la gauche, il va le faire avec les 
armes que lui donnent cet accord.

Il n’y a pas une seule avancée… sauf pour le patronat.

C’est un accord dont la principale caractéristique est de faciliter les 
licenciements et de rendre plus difficiles les recours des salariés, 
des IRP, des syndicats.

Mais il s’y ajoute une dizaine d’attaques:

- théoriques (contre la place de la procédure, contre celle du contrat individuel)
- dangereuses ( le CDI intermittent, la mobilité interne, le court circuit 
  des plans sociaux, les « accords de maintien de l’emploi » – de compétitivité..),
- en trompe l’oeil (contrats courts, temps partiel, formation professionnelle, 
  complémentaire santé)
- ou mesquines (blocage des dommages et intérêts aux prud’hommes, prescription 
  des heures supplémentaires après 3 ans..)
  
Tout ce qu’il y a dans l’accord va contre les salariés et pour les employeurs.

Il y a 8 syndicats en France : CGT, CFTD, FO, FSU, UNSA, SOLIDAIRES, CGC, 
CFTC. 

Seulement 5, CGT, CFDT, FO, CGC, CFTC ont été associés par le Medef aux 
négociations. Les trois syndicats qui ont signé, sont largement minoritaires.

Depuis 2008, la loi établit que ce n’est plus le nombre de syndicats qui 
signe qui établit la majorité et la validité d’un accord, ce n’est plus 
un « vote par ordre » mais un vote « par tête ». 

Il faut donc un seuil de représentativité en nombre de voix de salariés 
derrière les syndicats pour qu’un accord soit validé : ce seuil était 
fixé à 30 % jusqu’en 2012 et porté à 50 % en 2013.

Les trois directions CFDT, CGC, CFTC, étant totalement minoritaires sur 
ce coup auraient du ne pas faire bande à part, ne pas envisager de les 
signer. 

Tous leurs adhérents salariés devraient les pousser à ne pas les ratifier 
et à revenir dans un cadre d’unité syndicale !
