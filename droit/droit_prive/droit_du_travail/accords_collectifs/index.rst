
.. index::
   pair: Accords collectifs ; Droit du travail

.. _accords_collectifs:

===================
Accords collectifs 
===================

.. contents::
   :depth: 3

Introduction
============

.. seealso::
 
   - http://fr.wikipedia.org/wiki/Accords_collectifs_en_France
 
En France, l'État ne détient pas le monopole de la production des normes 
sociales.

En effet, le préambule de la Constitution de 1946 affirme que tout 
salarié « participe, par l'intermédiaire de ses délégués, à la 
détermination collective des conditions de travail », et l'article 
`L.2221-1`_ du code du travail français reconnaît " le droit des salariés à 
la négociation collective de l'ensemble de leurs conditions d'emploi et 
de travail et de leurs garanties sociales ".


.. _`L.2221-1`: http://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901659&dateTexte=20130127


Accords et conventions : distinction et généralités
====================================================

La loi établit une distinction entre la convention collective, qui 
détermine l'ensemble des conditions de travail et des garanties sociales, 
et l'accord collectif, qui ne porte que sur quelques-uns de ces sujets. 

Elle définit également la qualité des signataires des conventions et des 
accords collectifs.

La distinction entre accord et convention repose donc sur leur champ 
d'application. Ce champ d'application peut être géographique (national, 
régional ou local) ou/et professionnel (interprofessionnel, branche, entreprise).


.. _niveau_negociation:

Niveau de négociation
=====================

On distingue trois niveaux de négociation donnant lieu à trois types 
d'accord :

- Des Accords Nationaux Interprofessionnels (ANI)
- Des accords collectifs de branche (ordinaires ou étendus, nationaux ou 
  territoriaux)
- Des accords d'entreprise et accords d'établissement

Si l'accord a été négocié au niveau national et couvre l'ensemble des 
secteurs d'activité, on parlera d'ANI, 

s'il a été négocié au niveau  d'une branche d'activité, on parlera 
d'accord de branche, 

si l'accord a été conclu au niveau de l'entreprise, on parlera d'accord 
d'entreprise, etc.

Dans une déclaration commune du 16 juillet 2001, ratifiée par l'ensemble 
des organisations syndicales patronales et de salariés représentatives, 
portant sur les voies et moyens de l'approfondissement de la négociation 
collective, les partenaires sociaux définissent ainsi la fonction des 
différents niveaux d'accord : "Chaque niveau de négociation, national 
interprofessionnel, de branche et d'entreprise, assure des fonctions 
différentes dans le cadre d'un système organisé, destiné à conférer une 
pertinence optimale à la norme négociée tant dans ses effets que dans 
sa capacité à couvrir l'ensemble des salariés et des entreprises. 

Garant du système, le niveau national interprofessionnel doit assurer 
une cohérence d'ensemble. 

La branche joue un rôle structurant de solidarité, d'encadrement et 
d'impulsion de la négociation d'entreprise à travers l'existence de 
règles communes à la profession. 

La négociation d'entreprise permet de trouver et de mettre en œuvre des
solutions prenant directement en compte les caractéristiques et les 
besoins de chaque entreprise et de ses salariés."

Accords Nationaux Interprofessionnels
=====================================

.. toctree::
   :maxdepth: 3
   
   ani/index
   
   

