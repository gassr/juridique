
.. index::
   Droit privé

.. _droit_prive:

============
Droit privé 
============

.. seealso:: 

   - http://fr.wikipedia.org/wiki/Droit_priv%C3%A9

Introduction
============

Le droit privé est l'ensemble des règles qui régissent les rapports entre les 
personnes physiques ou morales. Les relations avec l'État ou l'Administration, 
et les relations des institutions publiques entre elles recouvrent le 
droit public.

Les principales branches du droit privé sont :

- le droit civil,
- le droit des affaires,
- le droit du travail.

En droit Français
=================

On oppose généralement le droit privé au droit public.

Le juge naturel du droit privé est le juge judiciaire. 

L'article 66 de la Constitution du 4 octobre 1958 a fait de l'ordre judiciaire 
le juge naturel des atteintes aux libertés fondamentales. 

Le **juge judiciaire est considéré comme le protecteur des libertés individuelles** 
et le garant de la propriété privée.


.. toctree::
   :maxdepth: 4
   
   droit_du_travail/index

   
