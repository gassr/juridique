

.. index::
   pair: Livre; La Justice hors la loi
   pair: Réfractions; La Justice hors la loi


.. _justice_hors_la_loi:

===========================================================
Réfractions automne 2016, N°37 **La Justice hors la loi**
===========================================================

.. seealso::

   - https://refractions.plusloin.org/spip.php?rubrique161
   - https://refractions.plusloin.org/spip.php?article1060


Introduction
===============

SI UN SLOGAN VOLONTIERS REPRIS PAR LES MANIFESTANTS ANARCHISTES est
« No justice, no peace » (pas de justice, pas de paix ; sous entendu de paix
sociale), alors il nous faut aborder la question de savoir ce que nous appelons
justice en anarchie.

Reprenant et télescopant des thématiques abordées dans les n°6 (De quel droit ?)
et n°31 (Les conflits c’est la vie), cette nouvelle parution s’attelle à
interroger cette question de la justice en anarchie, aussi bien sous l’angle
de ce qui peut fonder le sentiment de la justice, que sous l’angle pratique de
comment penser l’acte de juger.

Dès lors en effet que l’on n’abdique pas à une entité supérieure – l’État – la
fonction et le pouvoir de juger, et à la suite, le monopole de la violence
légitime (pour le maintien de l’ordre en amont et la mise en œuvre de la
sanction en aval), **à quelles questions une collectivité humaine doit-elle
répondre pour imaginer les mécanismes d’un système de justice ?**

Peut-on laisser aux seuls protagonistes d’un conflit le soin de le résoudre ou
bien l’intervention d’un tiers est-elle inéluctable, et dans ce cas selon
quelles modalités :

- simple médiation (faire se rapprocher les protagonistes au conflit),
- conciliation (proposer une solution au conflit)
- ou décision (imposer une solution au conflit) ?

Ce qui amène immédiatement la question de ce qui se joue dans l’acte de juger,
pour les protagonistes du conflit comme pour le corps social : le moment de la
justice est-il nécessairement postérieur au moment du droit ?

Autrement dit, y-a-t-il une règle abstraite préalablement
posée qui conduit à ce que le moment de la justice soit celui du traitement
de l’infraction à cette règle ? Ou bien est-ce le conflit lui-même qui,
heurtant quelques grands principes d’inspiration universelle (ne pas aTenter
à la vie ou à l’intégrité physique d’autrui, ne pas permettre qu’un individu
n’ait pas les moyens économiques et sociaux effectifs pour vivre en liberté
et à égalité avec les autres, dans une société donnée, etc.), va permettre de
produire une règle spécifique pour chaque situation de conflit ?
Quelle place fait-on à la transgression des règles qui peut être aussi un moyen
de les faire évoluer ?
Quel doit être le but de l’acte de juger si l’on écarte l’idée de punir ?

Ce qui amène aux débats autour de la justice dite transactionnelle ou réparatrice.

Au travers d’articles de fond, de compte-rendus d’expériences historiques ou
actuelles (chacune prise dans leur contexte singulier) ou encore de commentaires
d’ouvrages, ce numéro 37 tente de poser les termes possibles d’un débat que
les anarchistes ne peuvent éviter, pour dépasser l’image d’Épinal de l’anarchie
contre toutes les lois.
On peut à ce propos faire un parallèle avec la thèse de Karl Marx ainsi
résumée par Justine Lacroix et Jean-Yves Pranchère dans leur ouvrage
Le procès des droits de l’Homme (voir note de lecture infra) :
« Dans la phase supérieure de la société communiste, nous vivrons dans le
pays de cocagne de la société d’abondance, habitée par des hommes libres
de toute aliénation, où le droit ne serait en conséquence d’aucun usage. »

Dans une première partie, nous revenons sur les conceptions anarchistes du
droit et de la justice, qui ne se réduisent pas à une conception univoque,
mais qui ont toutes en commun de se distinguer de la loi imposée par l’État.

Il existe en effet la possibilité d’élaborer des règles collectivement, dans
une société exempte de l’exploitation capitaliste et de la domination étatique,
qui puissent être adéquates à une réelle justice.
Dans cette perspective, la conception d’une justice anarchiste ne renvoie pas
cependant à une harmonie vierge de tout conflit et de tout questionnement, ne
serait ce qu’en termes d’évaluation de la responsabilité individuelle.

C’est ce qui nous amène à la seconde partie, où nous abordons concrètement de
quelle manière la règle anarchiste peut être appréhendée dans ses multiples
dimensions. Il sera alors question de savoir si elle peut prendre la forme du
jeu, s’il est envisageable de concevoir le jugement dans une société libertaire
ou encore s’il est possible, voire nécessaire, de pouvoir en transgresser
les normes.

L’examen d’exemples comme le règlement des conflits au Rojava kurde, dans les
communautés indigènes d’Amérique centrale ou dans l’Espagne révolutionnaire
de 1936 nous permettront d’illustrer ces problématiques.

Enfin, dans une perspective critique du droit contemporain seront analysés le
droit de la médiation institutionnelle, qui neutralise les collectifs de
résistance, ainsi que l’utilisation abusive de la génétique comme administration
de la preuve, qui constituent quelques cas parmi d’autres représentant
**L’organisation de la Vindicte appelée Justice**, texte toujours d’actualité de
Kropotkine reproduit dans ce numéro.


Table des matières
====================

.. seealso::

   - https://refractions.plusloin.org/spip.php?article1060


Anarchives
============

.. seealso::

   - https://refractions.plusloin.org/IMG/pdf/37-anarchive.pdf

Deux textes de Pierre Kropotkine.







