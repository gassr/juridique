
.. index::
   pair: Juridique; Décrets

.. _juridique_decrets:

=========
Décrets
=========

Définition
==========


Un décret (du latin decretum, « décision ») est une décision émise par 
une autorité souveraine.

Actuellement, en France, un décret est une norme émanant du pouvoir 
réglementaire. Il est pris par le Premier ministre, éventuellement 
contresigné par les ministres concernés par son application, ou par le 
président de la République. 
Dans la hiérarchie des normes, il prend une valeur supérieure aux arrêtés.
	  
.. seealso::  https://fr.wikipedia.org/wiki/D%C3%A9cret  
  
  
Décrets par années
==================

.. toctree::
   :maxdepth: 4
   
   2009/index      
